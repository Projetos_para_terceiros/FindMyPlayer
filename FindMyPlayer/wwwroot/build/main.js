webpackJsonp([29],{

/***/ 12:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomError; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_app_module__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__BaseModel__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_sessao_sessao__ = __webpack_require__(14);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();



var CustomError = /** @class */ (function (_super) {
    __extends(CustomError, _super);
    function CustomError(init) {
        var _this = _super.call(this, __WEBPACK_IMPORTED_MODULE_0__app_app_module__["a" /* AppModule */].injector.get(__WEBPACK_IMPORTED_MODULE_2__providers_sessao_sessao__["a" /* SessaoProvider */])) || this;
        Object.assign(_this, init);
        return _this;
    }
    CustomError.prototype.getToJsonProperties = function () {
        return null;
    };
    return CustomError;
}(__WEBPACK_IMPORTED_MODULE_1__BaseModel__["a" /* BaseModel */]));

//# sourceMappingURL=CustomError.js.map

/***/ }),

/***/ 14:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SessaoProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__models_CustomError__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__global__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the SessaoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var SessaoProvider = /** @class */ (function () {
    function SessaoProvider(
        //private apiUsuario: UsuarioServiceProvider,
        events) {
        this.events = events;
    }
    SessaoProvider.prototype.getChave = function () {
        return this.chave;
    };
    SessaoProvider.prototype.tratarRequisicao = function (response) {
        var dado;
        try {
            dado = response.json();
        }
        catch (error) {
            throw new Error("Response error");
        }
        if (dado.status == "Erro") {
            var erro = new __WEBPACK_IMPORTED_MODULE_0__models_CustomError__["a" /* CustomError */](dado);
            if (erro.codigo == __WEBPACK_IMPORTED_MODULE_1__global__["a" /* GlobalVariable */].CODIGOS.CHAVE_INVALIDA) {
                this.refazerLogin();
                throw new __WEBPACK_IMPORTED_MODULE_0__models_CustomError__["a" /* CustomError */]({ codigo: __WEBPACK_IMPORTED_MODULE_1__global__["a" /* GlobalVariable */].CODIGOS.REPETIR_REQUISICAO });
            }
            else {
                throw new Error(erro.mensagem);
            }
        }
        else {
            if (dado.chaveSessao)
                this.atualizarChave(dado.chaveSessao);
        }
        return dado;
    };
    SessaoProvider.prototype.atualizarChave = function (novaChave) {
        this.chave = novaChave;
    };
    SessaoProvider.prototype.refazerLogin = function () {
        this.events.publish('sessao:refazer');
        // let usuarioLogado = this.apiUsuario.getUsuarioLogado();
        // this.apiUsuario.login(usuarioLogado.email, usuarioLogado.senhaHash).subscribe(
        //   (resp) => {
        //     if (resp) {
        //     }
        //     else {
        //       this.deslogar();
        //     }
        //   },
        //   (err) => { this.deslogar(); },
        //   () => { }
        // );
    };
    SessaoProvider.prototype.deslogar = function () {
        //this.apiUsuario.logout();
        this.events.publish('sessao:deslogar');
    };
    SessaoProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* Events */]])
    ], SessaoProvider);
    return SessaoProvider;
}());

//# sourceMappingURL=sessao.js.map

/***/ }),

/***/ 15:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GlobalVariable; });
var GlobalVariable = Object.freeze({
    //BASE_API_URL_teste: 'http://localhost:8981/api/',
    //BASE_API_URL: 'http://homologacao-findmyplayer.azurewebsites.net/api/',
    //BASE_API: 'http://fmp-homologa.azurewebsites.net/api/',
    //BASE_API: 'http://orion.myftp.org:6001/api/',
    BASE_API: 'http://findmyplayer.azurewebsites.net/api/',
    //CHAT_URL: 'ws://orion.myftp.org:6001/ws',
    // CHAT_URL: 'ws://findmyplayer.azurewebsites.net/ws',
    CHAT_URL: 'wss://findmyplayer.azurewebsites.net/ws',
    CODIGOS: {
        CHAVE_INVALIDA: 100,
        REPETIR_REQUISICAO: 55
    }
});
//# sourceMappingURL=global.js.map

/***/ }),

/***/ 162:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeagueoflegendsServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__models_DadosPartida__ = __webpack_require__(688);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_DadosNaoEstaticosUsuario__ = __webpack_require__(689);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__global__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__sessao_sessao__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__models_CustomError__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/*
  Generated class for the LeagueoflegendsServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var LeagueoflegendsServiceProvider = /** @class */ (function () {
    function LeagueoflegendsServiceProvider(http, sessao) {
        this.http = http;
        this.sessao = sessao;
        console.log('Hello LeagueoflegendsServiceProvider Provider');
    }
    LeagueoflegendsServiceProvider.prototype.getLeagueOfLegends = function () {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_6__global__["a" /* GlobalVariable */].BASE_API + "LeagueOfLegends")
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_8__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_8__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    LeagueoflegendsServiceProvider.prototype.getLeagueOfLegend = function (id) {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_6__global__["a" /* GlobalVariable */].BASE_API + "LeagueOfLegends/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_8__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_8__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    LeagueoflegendsServiceProvider.prototype.addLeagueOfLegend = function (lol) {
        var _this = this;
        var lolJson = lol.toJson();
        console.log(lolJson);
        var header = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        return this.http.post(__WEBPACK_IMPORTED_MODULE_6__global__["a" /* GlobalVariable */].BASE_API + "LeagueOfLegends/Cadastrar", lolJson, { headers: header })
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_8__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_8__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return _this.sessao.tratarRequisicao(response); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    LeagueoflegendsServiceProvider.prototype.putLeagueOfLegend = function (lol) {
        var _this = this;
        var lolJson = lol.toJson();
        console.log(lolJson);
        var header = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        return this.http.put(__WEBPACK_IMPORTED_MODULE_6__global__["a" /* GlobalVariable */].BASE_API + "LeagueOfLegends/Editar/" + lol.leagueOfLegendsId, lolJson, { headers: header })
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_8__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_8__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return _this.sessao.tratarRequisicao(response); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    LeagueoflegendsServiceProvider.prototype.GetTaxasUsuario = function (id) {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_6__global__["a" /* GlobalVariable */].BASE_API + "LeagueOfLegends/GetTaxasUsuario/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_8__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_8__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return new __WEBPACK_IMPORTED_MODULE_1__models_DadosNaoEstaticosUsuario__["a" /* DadosNaoEstaticosUsuario */](_this.sessao.tratarRequisicao(response)); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    LeagueoflegendsServiceProvider.prototype.getEspectador = function (id) {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_6__global__["a" /* GlobalVariable */].BASE_API + "LeagueOfLegends/Espectador/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_8__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return new __WEBPACK_IMPORTED_MODULE_0__models_DadosPartida__["a" /* DadosPartida */](_this.sessao.tratarRequisicao(response)); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    LeagueoflegendsServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_7__sessao_sessao__["a" /* SessaoProvider */]])
    ], LeagueoflegendsServiceProvider);
    return LeagueoflegendsServiceProvider;
}());

//# sourceMappingURL=leagueoflegends-service.js.map

/***/ }),

/***/ 179:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 179;

/***/ }),

/***/ 180:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WebsocketService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var WebsocketService = /** @class */ (function () {
    function WebsocketService() {
    }
    WebsocketService.prototype.connect = function (url) {
        if (!this.subject) {
            this.subject = this.create(url);
            console.log("Successfully connected: " + url);
        }
        return this.subject;
    };
    WebsocketService.prototype.create = function (url) {
        var ws = new WebSocket(url);
        var observable = __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__["Observable"].create(function (obs) {
            ws.onmessage = obs.next.bind(obs);
            ws.onerror = obs.error.bind(obs);
            ws.onclose = obs.complete.bind(obs);
            return ws.close.bind(ws);
        });
        var observer = {
            next: function (data) {
                if (ws.readyState === WebSocket.OPEN) {
                    ws.send(JSON.stringify(data));
                }
            }
        };
        return __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__["Subject"].create(observer, observable);
    };
    WebsocketService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], WebsocketService);
    return WebsocketService;
}());

//# sourceMappingURL=WebsocketService.js.map

/***/ }),

/***/ 31:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_websocket_service_WebsocketService__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_splash_screen__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_status_bar__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_storage__ = __webpack_require__(318);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_platform_browser_animations__ = __webpack_require__(715);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ionic2_rating__ = __webpack_require__(372);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__app_component__ = __webpack_require__(717);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__directives_autosize_autosize__ = __webpack_require__(718);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__providers_jogo_service_jogo_service__ = __webpack_require__(371);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_herois_service_herois_service__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__providers_item_service_item_service__ = __webpack_require__(368);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__providers_feitico_service_feitico_service__ = __webpack_require__(385);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__providers_runa_service_runa_service__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__providers_build_feitico_service_build_feitico_service__ = __webpack_require__(377);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__providers_build_runa_service_build_runa_service__ = __webpack_require__(375);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__providers_sessao_sessao__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__providers_usuario_service_usuario_service__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__providers_leagueoflegends_service_leagueoflegends_service__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__providers_lane_service_lane_service__ = __webpack_require__(719);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__providers_leagueoflegends_regioes_service_leagueoflegends_regioes_service__ = __webpack_require__(384);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__providers_relacionamento_service_relacionamento_service__ = __webpack_require__(370);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__providers_tipo_relacionamento_service_tipo_relacionamento_service__ = __webpack_require__(720);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__providers_chat_service__ = __webpack_require__(362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__providers_emoji__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__providers_build_service_build_service__ = __webpack_require__(367);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__providers_build_item_service_build_item_service__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__providers_counters_service_counters_service__ = __webpack_require__(378);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__providers_dicas_service_dicas_service__ = __webpack_require__(379);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__providers_projetos_projetos__ = __webpack_require__(721);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__providers_amizade_service_amizade_service__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__providers_avaliacao_service_avaliacao_service__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__providers_chat_grupo_service_chat_grupo_service__ = __webpack_require__(363);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__providers_time_service_time_service__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__angular_common_http__ = __webpack_require__(165);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__providers_build_favorita_service_build_favorita_service__ = __webpack_require__(380);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









































var AppModule = /** @class */ (function () {
    function AppModule(injector) {
        AppModule_1.injector = injector;
    }
    AppModule_1 = AppModule;
    AppModule = AppModule_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_12__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_13__directives_autosize_autosize__["a" /* AutosizeDirective */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["g" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_12__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/champion/champion-build/champion-build.module#ChampionBuildPageModule', name: 'ChampionBuildPage', segment: 'champion-build', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/champion/champion-counters/champion-counters.module#ChampionCountersPageModule', name: 'ChampionCountersPage', segment: 'champion-counters', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/champion/champion-info/champion-info.module#ChampionInfoPageModule', name: 'ChampionInfoPage', segment: 'champion-info', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/champion/champion-userbuilds/champion-userbuilds.module#ChampionUserbuildsPageModule', name: 'ChampionUserbuildsPage', segment: 'champion-userbuilds', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/champion/champion.module#ChampionPageModule', name: 'ChampionPage', segment: 'champion', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/chat-grupo/chat-grupo.module#ChatModule', name: 'ChatGrupoPage', segment: 'chat-grupo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/chat/chat.module#ChatModule', name: 'Chat', segment: 'chat', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/contatos-main/contatos-main.module#ContatosMainPageModule', name: 'ContatosMainPage', segment: 'contatos-main', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/formar-time/formar-time.module#FormarTimePageModule', name: 'FormarTimePage', segment: 'formar-time', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/home/home.module#HomePageModule', name: 'HomePage', segment: 'home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/lista-champions/lista-champions.module#ListaChampionsPageModule', name: 'ListaChampionsPage', segment: 'lista-champions', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/lol-account-modal/lol-account-modal.module#LolAccountModalPageModule', name: 'LolAccountModalPage', segment: 'lol-account-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/main/main.module#MainPageModule', name: 'MainPage', segment: 'main', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/match-main/match-main.module#MatchMainPageModule', name: 'MatchMainPage', segment: 'match-main', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/match-main/match/match.module#MatchPageModule', name: 'MatchPage', segment: 'match', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/match-main/meus-matches/meus-matches.module#MeusMatchesPageModule', name: 'MeusMatchesPage', segment: 'meus-matches', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/novo-grupo-modal/novo-grupo-modal.module#NovoGrupoModalPageModule', name: 'NovoGrupoModalPage', segment: 'novo-grupo-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/partidas/partidas.module#PartidasPageModule', name: 'PartidasPage', segment: 'partidas', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/salas/salas.module#SalasPageModule', name: 'SalasPage', segment: 'salas', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/selecionar-feitico-modal/selecionar-feitico-modal.module#SelecionarFeiticoModalPageModule', name: 'SelecionarFeiticoModalPage', segment: 'selecionar-feitico-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/selecionar-item-modal/selecionar-item-modal.module#SelecionarItemModalPageModule', name: 'SelecionarItemModalPage', segment: 'selecionar-item-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/selecionar-heroi-modal/selecionar-heroi-modal.module#SelecionarHeroiModalPageModule', name: 'SelecionarHeroiModalPage', segment: 'selecionar-heroi-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/selecionar-runa-modal/selecionar-runa-modal.module#SelecionarRunaModalPageModule', name: 'SelecionarRunaModalPage', segment: 'selecionar-runa-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tela-integracao/tela-integracao.module#TelaIntegracaoPageModule', name: 'TelaIntegracaoPage', segment: 'tela-integracao', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/user-profile/user-profile.module#UserProfilePageModule', name: 'UserProfilePage', segment: 'user-profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/usuario-game-lol-profile/usuario-game-lol-profile.module#UsuarioGameLolProfileModule', name: 'UsuarioGameLolProfilePage', segment: 'usuario-game-lol-profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/usuario-profile/usuario-profile.module#UsuarioProfilePageModule', name: 'UsuarioProfilePage', segment: 'usuario-profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/welcome/welcome.module#WelcomePageModule', name: 'WelcomePage', segment: 'welcome', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_9__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_8__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_39__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_10__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_11_ionic2_rating__["a" /* Ionic2RatingModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["e" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_12__app_component__["a" /* MyApp */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_4__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["f" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_14__providers_jogo_service_jogo_service__["a" /* JogoServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_15__providers_herois_service_herois_service__["a" /* HeroisServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_16__providers_item_service_item_service__["a" /* ItemServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_18__providers_runa_service_runa_service__["a" /* RunaServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_17__providers_feitico_service_feitico_service__["a" /* FeiticoServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_22__providers_usuario_service_usuario_service__["a" /* UsuarioServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_23__providers_leagueoflegends_service_leagueoflegends_service__["a" /* LeagueoflegendsServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_24__providers_lane_service_lane_service__["a" /* LaneServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_25__providers_leagueoflegends_regioes_service_leagueoflegends_regioes_service__["a" /* LeagueoflegendsRegioesServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_26__providers_relacionamento_service_relacionamento_service__["a" /* RelacionamentoServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_27__providers_tipo_relacionamento_service_tipo_relacionamento_service__["a" /* TipoRelacionamentoServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_30__providers_build_service_build_service__["a" /* BuildServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_31__providers_build_item_service_build_item_service__["a" /* BuildItemServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_19__providers_build_feitico_service_build_feitico_service__["a" /* BuildFeiticoServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_20__providers_build_runa_service_build_runa_service__["a" /* BuildRunaServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_30__providers_build_service_build_service__["a" /* BuildServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_32__providers_counters_service_counters_service__["a" /* CountersServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_storage__["a" /* IonicStorageModule */],
                __WEBPACK_IMPORTED_MODULE_33__providers_dicas_service_dicas_service__["a" /* DicasServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_34__providers_projetos_projetos__["a" /* ProjetosProvider */],
                __WEBPACK_IMPORTED_MODULE_28__providers_chat_service__["a" /* ChatService */],
                __WEBPACK_IMPORTED_MODULE_29__providers_emoji__["a" /* EmojiProvider */],
                __WEBPACK_IMPORTED_MODULE_21__providers_sessao_sessao__["a" /* SessaoProvider */],
                __WEBPACK_IMPORTED_MODULE_35__providers_amizade_service_amizade_service__["a" /* AmizadeServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_36__providers_avaliacao_service_avaliacao_service__["a" /* AvaliacaoServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_37__providers_chat_grupo_service_chat_grupo_service__["a" /* ChatGrupoServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_40__providers_build_favorita_service_build_favorita_service__["a" /* BuildFavoritaServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_38__providers_time_service_time_service__["a" /* TimeServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_0__providers_websocket_service_WebsocketService__["a" /* WebsocketService */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_4__angular_core__["CUSTOM_ELEMENTS_SCHEMA"]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__angular_core__["Injector"]])
    ], AppModule);
    return AppModule;
    var AppModule_1;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 317:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/champion/champion-build/champion-build.module": [
		722,
		19
	],
	"../pages/champion/champion-counters/champion-counters.module": [
		723,
		18
	],
	"../pages/champion/champion-info/champion-info.module": [
		724,
		17
	],
	"../pages/champion/champion-userbuilds/champion-userbuilds.module": [
		725,
		16
	],
	"../pages/champion/champion.module": [
		726,
		15
	],
	"../pages/chat-grupo/chat-grupo.module": [
		727,
		14
	],
	"../pages/chat/chat.module": [
		728,
		13
	],
	"../pages/contatos-main/contatos-main.module": [
		729,
		1
	],
	"../pages/formar-time/formar-time.module": [
		730,
		2
	],
	"../pages/home/home.module": [
		731,
		28
	],
	"../pages/lista-champions/lista-champions.module": [
		732,
		12
	],
	"../pages/lol-account-modal/lol-account-modal.module": [
		733,
		5
	],
	"../pages/main/main.module": [
		734,
		27
	],
	"../pages/match-main/match-main.module": [
		735,
		21
	],
	"../pages/match-main/match/match.module": [
		736,
		0
	],
	"../pages/match-main/meus-matches/meus-matches.module": [
		737,
		11
	],
	"../pages/novo-grupo-modal/novo-grupo-modal.module": [
		738,
		22
	],
	"../pages/partidas/partidas.module": [
		739,
		26
	],
	"../pages/register/register.module": [
		741,
		25
	],
	"../pages/salas/salas.module": [
		740,
		24
	],
	"../pages/selecionar-feitico-modal/selecionar-feitico-modal.module": [
		742,
		10
	],
	"../pages/selecionar-heroi-modal/selecionar-heroi-modal.module": [
		744,
		9
	],
	"../pages/selecionar-item-modal/selecionar-item-modal.module": [
		743,
		8
	],
	"../pages/selecionar-runa-modal/selecionar-runa-modal.module": [
		745,
		7
	],
	"../pages/tela-integracao/tela-integracao.module": [
		746,
		6
	],
	"../pages/user-profile/user-profile.module": [
		747,
		20
	],
	"../pages/usuario-game-lol-profile/usuario-game-lol-profile.module": [
		748,
		4
	],
	"../pages/usuario-profile/usuario-profile.module": [
		749,
		3
	],
	"../pages/welcome/welcome.module": [
		750,
		23
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 317;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 36:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaseModel; });
var BaseModel = /** @class */ (function () {
    function BaseModel(sessao) {
        this.sessao = sessao;
    }
    BaseModel.prototype.toJson = function () {
        this.chaveSessao = this.sessao.getChave();
        this.timeStamp = new Date;
        var jsonArray = this.getToJsonProperties();
        jsonArray.push('chaveSessao');
        jsonArray.push('timeStamp');
        return JSON.stringify(this, jsonArray);
    };
    return BaseModel;
}());

//# sourceMappingURL=BaseModel.js.map

/***/ }),

/***/ 360:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmojiProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/*
  Generated class for the EmojiProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var EmojiProvider = /** @class */ (function () {
    function EmojiProvider() {
    }
    EmojiProvider.prototype.getEmojis = function () {
        var EMOJIS = "😀 😃 😄 😁 😆 😅 😂 🤣 ☺️ 😊 😇 🙂 🙃 😉 😌 😍 😘 😗 😙 😚 😋 😜 😝 😛 🤑 🤗 🤓 😎 🤡 🤠 😏 😒 😞 😔 😟 😕 🙁" +
            " ☹️ 😣 😖 😫 😩 😤 😠 😡 😶 😐 😑 😯 😦 😧 😮 😲 😵 😳 😱 😨 😰 😢 😥 🤤 😭 😓 😪 😴 🙄 🤔 🤥 😬 🤐 🤢 🤧 😷 🤒 🤕 😈 👿" +
            " 👹 👺 💩 👻 💀 ☠️ 👽 👾 🤖 🎃 😺 😸 😹 😻 😼 😽 🙀 😿 😾 👐 🙌 👏 🙏 🤝 👍 👎 👊 ✊ 🤛 🤜 🤞 ✌️ 🤘 👌 👈 👉 👆 👇 ☝️ ✋ 🤚" +
            " 🖐 🖖 👋 🤙 💪 🖕 ✍️ 🤳 💅 🖖 💄 💋 👄 👅 👂 👃 👣 👁 👀 🗣 👤 👥 👶 👦 👧 👨 👩 👱‍♀️ 👱 👴 👵 👲 👳‍♀️ 👳 👮‍♀️ 👮 👷‍♀️ 👷" +
            " 💂‍♀️ 💂 🕵️‍♀️ 🕵️ 👩‍⚕️ 👨‍⚕️ 👩‍🌾 👨‍🌾 👩‍🍳 👨‍🍳 👩‍🎓 👨‍🎓 👩‍🎤 👨‍🎤 👩‍🏫 👨‍🏫 👩‍🏭 👨‍🏭 👩‍💻 👨‍💻 👩‍💼 👨‍💼 👩‍🔧 👨‍🔧 👩‍🔬 👨‍🔬" +
            " 👩‍🎨 👨‍🎨 👩‍🚒 👨‍🚒 👩‍✈️ 👨‍✈️ 👩‍🚀 👨‍🚀 👩‍⚖️ 👨‍⚖️ 🤶 🎅 👸 🤴 👰 🤵 👼 🤰 🙇‍♀️ 🙇 💁 💁‍♂️ 🙅 🙅‍♂️ 🙆 🙆‍♂️ 🙋 🙋‍♂️ 🤦‍♀️ 🤦‍♂️ 🤷‍♀" +
            "️ 🤷‍♂️ 🙎 🙎‍♂️ 🙍 🙍‍♂️ 💇 💇‍♂️ 💆 💆‍♂️ 🕴 💃 🕺 👯 👯‍♂️ 🚶‍♀️ 🚶 🏃‍♀️ 🏃 👫 👭 👬 💑 👩‍❤️‍👩 👨‍❤️‍👨 💏 👩‍❤️‍💋‍👩 👨‍❤️‍💋‍👨 👪 👨‍👩‍👧" +
            " 👨‍👩‍👧‍👦 👨‍👩‍👦‍👦 👨‍👩‍👧‍👧 👩‍👩‍👦 👩‍👩‍👧 👩‍👩‍👧‍👦 👩‍👩‍👦‍👦 👩‍👩‍👧‍👧 👨‍👨‍👦 👨‍👨‍👧 👨‍👨‍👧‍👦 👨‍👨‍👦‍👦 👨‍👨‍👧‍👧 👩‍👦 👩‍👧" +
            " 👩‍👧‍👦 👩‍👦‍👦 👩‍👧‍👧 👨‍👦 👨‍👧 👨‍👧‍👦 👨‍👦‍👦 👨‍👧‍👧 👚 👕 👖 👔 👗 👙 👘 👠 👡 👢 👞 👟 👒 🎩 🎓 👑 ⛑ 🎒 👝 👛 👜 💼 👓" +
            " 🕶 🌂 ☂️";
        var EmojiArr = EMOJIS.split(' ');
        var groupNum = Math.ceil(EmojiArr.length / (24));
        var items = [];
        for (var i = 0; i < groupNum; i++) {
            items.push(EmojiArr.slice(i * 24, (i + 1) * 24));
        }
        return items;
    };
    EmojiProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], EmojiProvider);
    return EmojiProvider;
}());

//# sourceMappingURL=emoji.js.map

/***/ }),

/***/ 361:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeroisServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_radix_trie_js__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_radix_trie_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_radix_trie_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__global__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__models_CustomError__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/*
  Generated class for the HeroisServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var HeroisServiceProvider = /** @class */ (function () {
    function HeroisServiceProvider(http) {
        this.http = http;
        console.log("Hello HeroisServiceProvider Provider");
        this.tree = new __WEBPACK_IMPORTED_MODULE_4_radix_trie_js__();
    }
    /**
     * @param  {number} id
     * @returns Observable
     */
    HeroisServiceProvider.prototype.getHeroi = function (id) {
        return this.http
            .get(__WEBPACK_IMPORTED_MODULE_5__global__["a" /* GlobalVariable */].BASE_API + "Herois/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_6__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error); })
            .map(function (data) { return data.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error || "Server error"); })
            .publishReplay(1)
            .refCount();
    };
    /**
     * @returns Observable
     */
    HeroisServiceProvider.prototype.getHerois = function () {
        var _this = this;
        if (!this.lista) {
            this.lista = this.http
                .get(__WEBPACK_IMPORTED_MODULE_5__global__["a" /* GlobalVariable */].BASE_API + "Herois")
                .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_6__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error); })
                .map(function (data) {
                var herois = data.json();
                // Adiciona os elementos na arvore
                herois.forEach(function (a) { return _this.tree.add(a.nome, a); });
                return herois;
            })
                .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error || "Server error"); });
        }
        return this.lista;
    };
    /**
     * @param  {string} query
     * @returns Observable
     */
    HeroisServiceProvider.prototype.getSearch = function (query) {
        var _this = this;
        return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].create(function (observer) {
            observer.next(query.length > 0
                ? Array.from(_this.tree.fuzzyGet(query)).map(function (a) { return a[1]; })
                : Array.from(_this.tree.values()));
            observer.complete();
        });
    };
    HeroisServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], HeroisServiceProvider);
    return HeroisServiceProvider;
}());

//# sourceMappingURL=herois-service.js.map

/***/ }),

/***/ 362:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__usuario_service_usuario_service__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__global__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__sessao_sessao__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__models_ChatMessages__ = __webpack_require__(369);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__models_CustomError__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__websocket_service_WebsocketService__ = __webpack_require__(180);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var ChatService = /** @class */ (function () {
    function ChatService(http, events, sessao, apiUsuario, wsService) {
        this.http = http;
        this.events = events;
        this.sessao = sessao;
        this.apiUsuario = apiUsuario;
        this.wsService = wsService;
    }
    ChatService.prototype.connect = function (user) {
        var _this = this;
        if (this.user) {
            console.log("Já conectado ao chat");
            return;
        }
        this.user = user;
        this.messages = this.wsService
            .connect(__WEBPACK_IMPORTED_MODULE_5__global__["a" /* GlobalVariable */].CHAT_URL)
            .map(function (response) {
            return response.data;
        })
            .catch(function (e, o) {
            console.log("Erro na conexao do socket", e);
            _this.user = null;
            return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw("Server error");
        });
        this.messages.subscribe(function (msgRecebida) {
            if (msgRecebida.indexOf("new_message") !== -1) {
                var split_1 = msgRecebida.split(":");
                if (split_1[1] == "g") {
                    _this.GetMessagesGroupo(split_1[2])
                        .subscribe(function (msgs) {
                        msgs.forEach(function (element) {
                            _this.events.publish("chat:received:grupo:" + split_1[2], element, Date.now());
                        });
                    });
                }
                if (split_1[1] == "u") {
                    _this.GetMessagesFromTo(split_1[2], _this.user.usuarioId)
                        .subscribe(function (msgs) {
                        msgs.forEach(function (element) {
                            _this.events.publish("chat:received:usuario:" + split_1[2], element, Date.now());
                        });
                    });
                }
            }
        });
        this.messages.next(user.usuarioId);
        // connect
        // this.ws = new $WebSocket(GlobalVariable.CHAT_URL);
        // this.ws.send(user.usuarioId).subscribe(
        //   msg => {
        //     console.log("next", msg.data);
        //   },
        //   msg => {
        //     console.log("error", msg);
        //   },
        //   () => {
        //     console.log("complete");
        //   }
        // );
        // // registrar callback
        // this.ws.onMessage(
        //   (msg: MessageEvent) => {
        //     if (msg.data.indexOf("new_message") !== -1) {
        //       let split = msg.data.split(":");
        //       if (split[1] == "g") {
        //         this.GetMessagesGroupo(split[2])
        //           .subscribe(
        //             (msgs) => {
        //               msgs.forEach(element => {
        //                 this.events.publish("chat:received:grupo:" + split[2], element, Date.now());
        //               });
        //             }
        //           );
        //       }
        //       if (split[1] == "u") {
        //         this.GetMessagesFromTo(split[2], this.user.usuarioId)
        //           .subscribe(
        //             (msgs) => {
        //               msgs.forEach(element => {
        //                 this.events.publish("chat:received:usuario:" + split[2], element, Date.now());
        //               });
        //             }
        //           );
        //       }
        //     }
        //   },
        //   { autoApply: false }
        // );
    };
    ChatService.prototype.joinGroupChat = function (toGrupo) {
        if (!this.user) {
            throw 'Nenhum Usuário conectado';
        }
        this.toGrupo = toGrupo;
    };
    ChatService.prototype.joinUserChat = function (toUser) {
        if (!this.user) {
            throw 'Nenhum Usuário conectado';
        }
        this.toUser = toUser;
    };
    // mockNewMsg(msg) {
    //   const mockMsg = new ChatMessages ({
    //     chatId: Date.now().toString(),
    //     usuario1Id: "210000198410281948",
    //     userName: "Hancock",
    //     userAvatar: "./assets/to-user.jpg",
    //     usuario2Id: "140000198202211138",
    //     time: new Date(),
    //     message: msg.message,
    //     status: "success"
    //   });
    //   setTimeout(() => {
    //     this.events.publish("chat:received", mockMsg, Date.now());
    //   }, Math.random() * 1800);
    // }
    // getMsgList(): Promise<ChatMessages[]> {
    //   const msgListUrl = "./assets/mock/msg-list.json";
    //   return this.http
    //     .get(msgListUrl)
    //     .toPromise()
    //     .then(response => response.json().array as ChatMessages[])
    //     .catch(err => Promise.reject(err || "err"));
    // }
    //  sendMsg(msg: ChatMessages): Observable<any> {
    // send with default send mode (now default send mode is Observer)
    // return this.ws.send(msg.message).map(
    //   msg => {
    //     console.log("Recebido", msg.data);
    //   },
    //   msg => {
    //     console.log("error", msg);
    //   },
    //   () => {
    //     console.log("complete");
    //   }
    // );
    //  }
    // getUserInfo(): Promise<UserInfo> {
    //   const userInfo: UserInfo = {
    //     id: "140000198202211138",
    //     name: "Luff",
    //     avatar: "./assets/user.jpg"
    //   };
    //   return new Promise(resolve => resolve(userInfo));
    // }
    // Requisicoes -----------------------------------------------------------
    ChatService.prototype.GetChatMessages = function () {
        var _this = this;
        this.http
            .get(__WEBPACK_IMPORTED_MODULE_5__global__["a" /* GlobalVariable */].BASE_API + "ChatMessages/GetChatMessages")
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_8__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (data) {
            return new __WEBPACK_IMPORTED_MODULE_7__models_ChatMessages__["a" /* ChatMessages */](_this.sessao.tratarRequisicao(data));
        })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || "Server error"); });
    };
    ChatService.prototype.GetAllMessagesReceiveByUser = function (id) {
        var _this = this;
        this.http
            .get(__WEBPACK_IMPORTED_MODULE_5__global__["a" /* GlobalVariable */].BASE_API + "ChatMessages/GetAllMessagesReceiveByUser/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_8__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (data) {
            var msgs = _this.sessao.tratarRequisicao(data)[0];
            msgs.forEach(function (element) {
                _this.events.publish("chat:received", element, Date.now());
            });
        })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || "Server error"); });
    };
    ChatService.prototype.GetAllMessagesSendByUser = function (id) {
        var _this = this;
        this.http
            .get(__WEBPACK_IMPORTED_MODULE_5__global__["a" /* GlobalVariable */].BASE_API + "ChatMessages/GetAllMessagesSendByUser/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_8__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (data) {
            var msgs = _this.sessao.tratarRequisicao(data)[0];
            msgs.forEach(function (element) {
                _this.events.publish("chat:received", element, Date.now());
            });
        })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || "Server error"); });
    };
    ChatService.prototype.GetMessagesFromTo = function (idFrom, idTo) {
        var _this = this;
        return this.http
            .get(__WEBPACK_IMPORTED_MODULE_5__global__["a" /* GlobalVariable */].BASE_API + "ChatMessages/GetMessagesFromTo/" + idFrom + "&" + idTo)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_8__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (data) {
            return _this.sessao.tratarRequisicao(data)[0];
        })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || "Server error"); });
    };
    // post
    ChatService.prototype.SendMessage = function (msg) {
        var header = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        var mensagem = msg.toJson();
        return this.http
            .post(__WEBPACK_IMPORTED_MODULE_5__global__["a" /* GlobalVariable */].BASE_API + "ChatMessages/SendMessage", mensagem, { headers: header })
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_8__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (data) {
        })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || "Server error"); });
    };
    //delete
    ChatService.prototype.DeleteChatMessagess = function (id) {
        this.http
            .delete(__WEBPACK_IMPORTED_MODULE_5__global__["a" /* GlobalVariable */].BASE_API + "ChatMessages/GetChatMessagess/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_8__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (data) {
        })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || "Server error"); });
    };
    ChatService.prototype.GetMessagesGroupo = function (grupoId) {
        var _this = this;
        return this.http
            .get(__WEBPACK_IMPORTED_MODULE_5__global__["a" /* GlobalVariable */].BASE_API + "ChatMessages/GetMessagesGroupo/" + grupoId)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_8__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (data) {
            return _this.sessao.tratarRequisicao(data);
        })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || "Server error"); });
    };
    ChatService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_6__sessao_sessao__["a" /* SessaoProvider */], __WEBPACK_IMPORTED_MODULE_0__usuario_service_usuario_service__["a" /* UsuarioServiceProvider */], __WEBPACK_IMPORTED_MODULE_9__websocket_service_WebsocketService__["a" /* WebsocketService */]])
    ], ChatService);
    return ChatService;
}());

//# sourceMappingURL=chat-service.js.map

/***/ }),

/***/ 363:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatGrupoServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__usuario_service_usuario_service__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__sessao_sessao__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__global__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__models_CustomError__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/*
  Generated class for the ChatGrupoServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ChatGrupoServiceProvider = /** @class */ (function () {
    function ChatGrupoServiceProvider(http, events, sessao, apiUsuario) {
        this.http = http;
        this.events = events;
        this.sessao = sessao;
        this.apiUsuario = apiUsuario;
        console.log('Hello ChatGrupoServiceProvider Provider');
    }
    // join(user: Usuario, grupo: ChatGrupo) {
    //   this.user = user;
    //   this.grupo = grupo;
    //   // connect
    //   this.ws = new $WebSocket(GlobalVariable.CHAT_URL);
    //   this.ws.onMessage(
    //     (msg: MessageEvent) => {
    //       if (msg.data === "new_message") {
    //         this.GetMessagesFromTo(this.grupo.usuarioId, this.user.usuarioId)
    //         .subscribe(
    //           (msgs) => {
    //             msgs.forEach(element => {
    //               this.events.publish("chat:received", element, Date.now());
    //             });
    //           }
    //         );
    //       }
    //     },
    //     { autoApply: false }
    //   );
    //   this.ws.send(user.usuarioId).subscribe(
    //     msg => {
    //       console.log("next", msg.data);
    //     },
    //     msg => {
    //       console.log("error", msg);
    //     },
    //     () => {
    //       console.log("complete");
    //     }
    //   );
    // }
    ChatGrupoServiceProvider.prototype.getChatGrupos = function () {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_6__global__["a" /* GlobalVariable */].BASE_API + "ChatGrupos")
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_7__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return _this.sessao.tratarRequisicao(response); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    ChatGrupoServiceProvider.prototype.getChatGrupo = function (id) {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_6__global__["a" /* GlobalVariable */].BASE_API + "ChatGrupos/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_7__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return _this.sessao.tratarRequisicao(response); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    ChatGrupoServiceProvider.prototype.getChatGruposUsuario = function (idUsuario, privado) {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_6__global__["a" /* GlobalVariable */].BASE_API + "ChatGrupos/GetChatGruposUsuario/" + idUsuario + '&' + privado)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_7__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return _this.sessao.tratarRequisicao(response); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    ChatGrupoServiceProvider.prototype.addChatGrupo = function (grupo, userId) {
        var _this = this;
        var grupoJson = grupo.toJson();
        console.log(grupoJson);
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        return this.http.post(__WEBPACK_IMPORTED_MODULE_6__global__["a" /* GlobalVariable */].BASE_API + "ChatGrupos/" + userId, grupoJson, { headers: header })
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_7__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return _this.sessao.tratarRequisicao(response); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ChatGrupoServiceProvider.prototype.editarChatGrupo = function (grupo) {
        var _this = this;
        var grupoJson = grupo.toJson();
        console.log(grupoJson);
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        return this.http.put(__WEBPACK_IMPORTED_MODULE_6__global__["a" /* GlobalVariable */].BASE_API + "ChatGrupos/" + grupo.chatGrupoId, grupoJson, { headers: header })
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_7__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return _this.sessao.tratarRequisicao(response); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    ChatGrupoServiceProvider.prototype.deleteChatGrupo = function (id) {
        var _this = this;
        return this.http.delete(__WEBPACK_IMPORTED_MODULE_6__global__["a" /* GlobalVariable */].BASE_API + "ChatGrupos/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_7__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return _this.sessao.tratarRequisicao(response); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    ChatGrupoServiceProvider.prototype.insereMembroNoGrupo = function (grupo) {
        var _this = this;
        var grupoJson = grupo.toJson();
        console.log(grupoJson);
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        return this.http.post(__WEBPACK_IMPORTED_MODULE_6__global__["a" /* GlobalVariable */].BASE_API + "ChatGrupos/InserirMembro", grupoJson, { headers: header })
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_7__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return _this.sessao.tratarRequisicao(response); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    ChatGrupoServiceProvider.prototype.removeMembroDoGrupo = function (userId, grupoId) {
        var _this = this;
        return this.http.delete(__WEBPACK_IMPORTED_MODULE_6__global__["a" /* GlobalVariable */].BASE_API + "ChatGrupos/" + userId + '&' + grupoId)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_7__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return _this.sessao.tratarRequisicao(response); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    ChatGrupoServiceProvider.prototype.ListarMembros = function (grupoId) {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_6__global__["a" /* GlobalVariable */].BASE_API + "ChatGrupos/ListarMembros/" + grupoId)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_7__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return _this.sessao.tratarRequisicao(response); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    ChatGrupoServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* Events */],
            __WEBPACK_IMPORTED_MODULE_5__sessao_sessao__["a" /* SessaoProvider */],
            __WEBPACK_IMPORTED_MODULE_4__usuario_service_usuario_service__["a" /* UsuarioServiceProvider */]])
    ], ChatGrupoServiceProvider);
    return ChatGrupoServiceProvider;
}());

//# sourceMappingURL=chat-grupo-service.js.map

/***/ }),

/***/ 364:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Usuario; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_sessao_sessao__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__BaseModel__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(31);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();



var Usuario = /** @class */ (function (_super) {
    __extends(Usuario, _super);
    function Usuario(init) {
        var _this = _super.call(this, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].injector.get(__WEBPACK_IMPORTED_MODULE_0__providers_sessao_sessao__["a" /* SessaoProvider */])) || this;
        _this.tipoUsuario = 0;
        Object.assign(_this, init);
        _this.matched = false;
        return _this;
    }
    Usuario.prototype.getToJsonProperties = function () {
        return [
            'usuarioId',
            'email',
            'nome',
            'senhaHash',
            'leagueOfLegendsId',
            'tipoAtivacaoId',
        ];
    };
    return Usuario;
}(__WEBPACK_IMPORTED_MODULE_1__BaseModel__["a" /* BaseModel */]));

//# sourceMappingURL=Usuario.js.map

/***/ }),

/***/ 366:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BuildItemServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__global__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__models_CustomError__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/*
  Generated class for the BuildItemServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var BuildItemServiceProvider = /** @class */ (function () {
    function BuildItemServiceProvider(http) {
        this.http = http;
        console.log('Hello BuildItemServiceProvider Provider');
    }
    BuildItemServiceProvider.prototype.getBuildItem = function (id) {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_0__global__["a" /* GlobalVariable */].BASE_API + "BuildItens/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_5__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].throw(error); })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    BuildItemServiceProvider.prototype.getBuildItems = function () {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_0__global__["a" /* GlobalVariable */].BASE_API + "BuildItens")
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_5__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].throw(error); })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    BuildItemServiceProvider.prototype.addBuild = function (build) {
        var item = JSON.stringify(build, [
            'itemId',
            'buildId'
        ]);
        var header = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        return this.http.post(__WEBPACK_IMPORTED_MODULE_0__global__["a" /* GlobalVariable */].BASE_API + "BuildItens", item, { headers: header })
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_5__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) {
            return response.json();
        })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    BuildItemServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Http */]])
    ], BuildItemServiceProvider);
    return BuildItemServiceProvider;
}());

//# sourceMappingURL=build-item-service.js.map

/***/ }),

/***/ 367:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BuildServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__sessao_sessao__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__global__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_Build__ = __webpack_require__(376);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__models_CustomError__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/*
  Generated class for the BuildServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var BuildServiceProvider = /** @class */ (function () {
    function BuildServiceProvider(http, sessao) {
        this.http = http;
        this.sessao = sessao;
        console.log('Hello BuildServiceProvider Provider');
    }
    BuildServiceProvider.prototype.getBuilds = function () {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_1__global__["a" /* GlobalVariable */].BASE_API + "Builds")
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_7__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return _this.sessao.tratarRequisicao(response); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    BuildServiceProvider.prototype.getBuildsHeroi = function (id) {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_1__global__["a" /* GlobalVariable */].BASE_API + "Builds/BuildsHeroi/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_7__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return _this.sessao.tratarRequisicao(response); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    BuildServiceProvider.prototype.getBuild = function (id) {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_1__global__["a" /* GlobalVariable */].BASE_API + "Builds/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_7__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return new __WEBPACK_IMPORTED_MODULE_3__models_Build__["a" /* Build */](_this.sessao.tratarRequisicao(response)); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    BuildServiceProvider.prototype.addBuild = function (build) {
        var _this = this;
        var usu = build.toJson();
        var header = new __WEBPACK_IMPORTED_MODULE_5__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        return this.http.post(__WEBPACK_IMPORTED_MODULE_1__global__["a" /* GlobalVariable */].BASE_API + "Builds", usu, { headers: header })
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_7__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return new __WEBPACK_IMPORTED_MODULE_3__models_Build__["a" /* Build */](_this.sessao.tratarRequisicao(response)); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    BuildServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_0__sessao_sessao__["a" /* SessaoProvider */]])
    ], BuildServiceProvider);
    return BuildServiceProvider;
}());

//# sourceMappingURL=build-service.js.map

/***/ }),

/***/ 368:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ItemServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_radix_trie_js__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_radix_trie_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_radix_trie_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__global__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__models_CustomError__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/*
  Generated class for the ItemServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var ItemServiceProvider = /** @class */ (function () {
    function ItemServiceProvider(http) {
        this.http = http;
        console.log("Hello ItemServiceProvider Provider");
    }
    ItemServiceProvider.prototype.getItems = function () {
        var _this = this;
        if (this.tree)
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].create(function (observer) {
                observer.next(Array.from(_this.tree.values()));
                observer.complete();
            });
        return this.http
            .get(__WEBPACK_IMPORTED_MODULE_5__global__["a" /* GlobalVariable */].BASE_API + "Itens")
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_6__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error); })
            .map(function (data) {
            var items = data.json();
            _this.tree = new __WEBPACK_IMPORTED_MODULE_4_radix_trie_js__();
            // Adiciona os elementos na arvore
            items.forEach(function (a) {
                if (!a.nome) {
                    console.log(a);
                }
                else {
                    _this.tree.add(a.nome, a);
                }
            });
            return items;
        })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error || "Server error"); });
    };
    ItemServiceProvider.prototype.getItem = function (id) {
        return this.http
            .get(__WEBPACK_IMPORTED_MODULE_5__global__["a" /* GlobalVariable */].BASE_API + "Itens/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_6__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error); })
            .map(function (response) { return response.json(); })
            .catch(function (error) {
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || "Server error");
        });
    };
    /**
     * @param  {string} query
     * @returns Observable
     */
    ItemServiceProvider.prototype.getSearch = function (query) {
        var _this = this;
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].create(function (observer) {
            observer.next(query.length > 0
                ? Array.from(_this.tree.fuzzyGet(query)).map(function (a) { return a[1]; })
                : Array.from(_this.tree.values()));
            observer.complete();
        });
    };
    ItemServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], ItemServiceProvider);
    return ItemServiceProvider;
}());

//# sourceMappingURL=item-service.js.map

/***/ }),

/***/ 369:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatMessages; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_app_module__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_sessao_sessao__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__BaseModel__ = __webpack_require__(36);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();



var ChatMessages = /** @class */ (function (_super) {
    __extends(ChatMessages, _super);
    function ChatMessages(init) {
        var _this = _super.call(this, __WEBPACK_IMPORTED_MODULE_0__app_app_module__["a" /* AppModule */].injector.get(__WEBPACK_IMPORTED_MODULE_1__providers_sessao_sessao__["a" /* SessaoProvider */])) || this;
        Object.assign(_this, init);
        return _this;
    }
    ChatMessages.prototype.getToJsonProperties = function () {
        return [
            "usuario1Id",
            "usuario2Id",
            "chatGrupoId",
            "message",
            "time"
        ];
    };
    return ChatMessages;
}(__WEBPACK_IMPORTED_MODULE_2__BaseModel__["a" /* BaseModel */]));

//# sourceMappingURL=ChatMessages.js.map

/***/ }),

/***/ 370:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RelacionamentoServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__global__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__sessao_sessao__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__models_CustomError__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/*
  Generated class for the RelacionamentoServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var RelacionamentoServiceProvider = /** @class */ (function () {
    function RelacionamentoServiceProvider(http, sessao) {
        this.http = http;
        this.sessao = sessao;
        console.log('Hello RelacionamentoServiceProvider Provider');
    }
    RelacionamentoServiceProvider.prototype.getFavoritadas = function (id) {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_0__global__["a" /* GlobalVariable */].BASE_API + "Relacionamentos/GetFavoritadas/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_6__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (data) { return _this.sessao.tratarRequisicao(data); })
            .catch(function (error) {
            return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || 'Server error');
        });
    };
    RelacionamentoServiceProvider.prototype.getRelacionamentos = function () {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_0__global__["a" /* GlobalVariable */].BASE_API + "Relacionamentos")
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_6__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (data) { return _this.sessao.tratarRequisicao(data); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    RelacionamentoServiceProvider.prototype.getRelacionamento = function (id) {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_0__global__["a" /* GlobalVariable */].BASE_API + "Relacionamentos/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_6__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (data) { return _this.sessao.tratarRequisicao(data); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    RelacionamentoServiceProvider.prototype.getNaoRelacionado = function (id) {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_0__global__["a" /* GlobalVariable */].BASE_API + "Relacionamentos/GetNaoRelacionados/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_6__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (data) { return _this.sessao.tratarRequisicao(data); })
            .catch(function (error) {
            return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || 'Server error');
        });
    };
    RelacionamentoServiceProvider.prototype.addRelacionamento = function (relacionamento) {
        var _this = this;
        var rel = relacionamento.toJson();
        var header = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        return this.http.post(__WEBPACK_IMPORTED_MODULE_0__global__["a" /* GlobalVariable */].BASE_API + "Relacionamentos", rel, { headers: header })
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_6__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (data) { return _this.sessao.tratarRequisicao(data); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    RelacionamentoServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_5__sessao_sessao__["a" /* SessaoProvider */]])
    ], RelacionamentoServiceProvider);
    return RelacionamentoServiceProvider;
}());

//# sourceMappingURL=relacionamento-service.js.map

/***/ }),

/***/ 371:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JogoServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_toPromise__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__models_CustomError__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/*
  Generated class for the JogoServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var JogoServiceProvider = /** @class */ (function () {
    function JogoServiceProvider(http) {
        this.http = http;
        this.apiJogo = 'api/jogos';
        console.log('Hello JogoServiceProvider Provider');
    }
    JogoServiceProvider.prototype.getJogos = function () {
        return this.http.get(this.apiJogo)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_5__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_0_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_0_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return response.json().data; })
            .catch(this.handleError);
    };
    JogoServiceProvider.prototype.handleError = function (error) {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    JogoServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]])
    ], JogoServiceProvider);
    return JogoServiceProvider;
}());

//# sourceMappingURL=jogo-service.js.map

/***/ }),

/***/ 373:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AvaliacaoServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__models_CustomError__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sessao_sessao__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_Avaliacao__ = __webpack_require__(374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__global__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/*
  Generated class for the AvaliacaoServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var AvaliacaoServiceProvider = /** @class */ (function () {
    function AvaliacaoServiceProvider(http, sessao) {
        this.http = http;
        this.sessao = sessao;
        console.log('Hello AvaliacaoServiceProvider Provider');
    }
    AvaliacaoServiceProvider.prototype.getAvaliacaoUsuarios = function () {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_5__global__["a" /* GlobalVariable */].BASE_API + "AvaliacaoUsuarios")
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_0__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return _this.sessao.tratarRequisicao(response); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    AvaliacaoServiceProvider.prototype.getAvaliacoesUsuario = function (id) {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_5__global__["a" /* GlobalVariable */].BASE_API + "AvaliacaoUsuarios/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_0__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return _this.sessao.tratarRequisicao(response); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    AvaliacaoServiceProvider.prototype.addAvaliacao = function (lol) {
        var _this = this;
        var lolJson = lol.toJson();
        console.log(lolJson);
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        return this.http.post(__WEBPACK_IMPORTED_MODULE_5__global__["a" /* GlobalVariable */].BASE_API + "AvaliacaoUsuarios", lolJson, { headers: header })
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_0__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return new __WEBPACK_IMPORTED_MODULE_3__models_Avaliacao__["a" /* Avaliacao */](_this.sessao.tratarRequisicao(response)); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    AvaliacaoServiceProvider.prototype.deleteAvaliacao = function (id) {
        var _this = this;
        return this.http.delete(__WEBPACK_IMPORTED_MODULE_5__global__["a" /* GlobalVariable */].BASE_API + "AvaliacaoUsuarios/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_0__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return _this.sessao.tratarRequisicao(response); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    AvaliacaoServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_6__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_2__sessao_sessao__["a" /* SessaoProvider */]])
    ], AvaliacaoServiceProvider);
    return AvaliacaoServiceProvider;
}());

//# sourceMappingURL=avaliacao-service.js.map

/***/ }),

/***/ 374:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Avaliacao; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_sessao_sessao__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_app_module__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__BaseModel__ = __webpack_require__(36);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();



var Avaliacao = /** @class */ (function (_super) {
    __extends(Avaliacao, _super);
    function Avaliacao(init) {
        var _this = _super.call(this, __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].injector.get(__WEBPACK_IMPORTED_MODULE_0__providers_sessao_sessao__["a" /* SessaoProvider */])) || this;
        Object.assign(_this, init);
        return _this;
    }
    Avaliacao.prototype.getToJsonProperties = function () {
        return [
            'estrelas',
            'avaliacao',
            'usuarioAvaliadorId',
            'usuarioAvaliadoId',
            'dataHora'
        ];
    };
    return Avaliacao;
}(__WEBPACK_IMPORTED_MODULE_2__BaseModel__["a" /* BaseModel */]));

//# sourceMappingURL=Avaliacao.js.map

/***/ }),

/***/ 375:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BuildRunaServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__global__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__models_CustomError__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/*
  Generated class for the BuildRunaServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var BuildRunaServiceProvider = /** @class */ (function () {
    function BuildRunaServiceProvider(http) {
        this.http = http;
        console.log('Hello BuildRunaServiceProvider Provider');
    }
    BuildRunaServiceProvider.prototype.getBuildRuna = function (id) {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_0__global__["a" /* GlobalVariable */].BASE_API + "BuildRunas/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_5__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].throw(error); })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    BuildRunaServiceProvider.prototype.getBuildRunas = function () {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_0__global__["a" /* GlobalVariable */].BASE_API + "BuildRunas")
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_5__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].throw(error); })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    BuildRunaServiceProvider.prototype.addBuild = function (build) {
        var runa = JSON.stringify(build, [
            'runaId',
            'buildId'
        ]);
        var header = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        return this.http.post(__WEBPACK_IMPORTED_MODULE_0__global__["a" /* GlobalVariable */].BASE_API + "BuildRunas", runa, { headers: header })
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_5__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) {
            return response.json();
        })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    BuildRunaServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Http */]])
    ], BuildRunaServiceProvider);
    return BuildRunaServiceProvider;
}());

//# sourceMappingURL=build-runa-service.js.map

/***/ }),

/***/ 376:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Build; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__BaseModel__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_sessao_sessao__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(31);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();



var Build = /** @class */ (function (_super) {
    __extends(Build, _super);
    function Build(init) {
        var _this = _super.call(this, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].injector.get(__WEBPACK_IMPORTED_MODULE_1__providers_sessao_sessao__["a" /* SessaoProvider */])) || this;
        _this.listaItems = [];
        Object.assign(_this, init);
        return _this;
    }
    Build.prototype.getToJsonProperties = function () {
        return [
            'nome',
            'descricao',
            'usuarioId',
            'heroiId',
        ];
    };
    return Build;
}(__WEBPACK_IMPORTED_MODULE_0__BaseModel__["a" /* BaseModel */]));

//# sourceMappingURL=Build.js.map

/***/ }),

/***/ 377:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BuildFeiticoServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__global__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_BuildFeitico__ = __webpack_require__(693);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__sessao_sessao__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__models_CustomError__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/*
  Generated class for the BuildFeiticoServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var BuildFeiticoServiceProvider = /** @class */ (function () {
    function BuildFeiticoServiceProvider(http, sessao) {
        this.http = http;
        this.sessao = sessao;
        console.log('Hello BuildFeiticoServiceProvider Provider');
    }
    BuildFeiticoServiceProvider.prototype.getBuildFeitico = function (id) {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_0__global__["a" /* GlobalVariable */].BASE_API + "BuildFeiticos/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_7__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].throw(error); })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    BuildFeiticoServiceProvider.prototype.getBuildFeiticos = function () {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_0__global__["a" /* GlobalVariable */].BASE_API + "BuildFeiticos")
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_7__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].throw(error); })
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    BuildFeiticoServiceProvider.prototype.addBuild = function (build) {
        var feitico = new __WEBPACK_IMPORTED_MODULE_2__models_BuildFeitico__["a" /* BuildFeitico */](build);
        var header = new __WEBPACK_IMPORTED_MODULE_4__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        return this.http.post(__WEBPACK_IMPORTED_MODULE_0__global__["a" /* GlobalVariable */].BASE_API + "BuildFeiticos", feitico, { headers: header })
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_7__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) {
            return response.json();
        })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    BuildFeiticoServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_6__sessao_sessao__["a" /* SessaoProvider */]])
    ], BuildFeiticoServiceProvider);
    return BuildFeiticoServiceProvider;
}());

//# sourceMappingURL=build-feitico-service.js.map

/***/ }),

/***/ 378:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CountersServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__global__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__models_CustomError__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/*
  Generated class for the CountersServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var CountersServiceProvider = /** @class */ (function () {
    function CountersServiceProvider(http) {
        this.http = http;
        console.log('Hello CountersServiceProvider Provider');
    }
    CountersServiceProvider.prototype.getCounter = function (heroiId) {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_0__global__["a" /* GlobalVariable */].BASE_API + "counters/" + heroiId)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_5__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].throw(error); })
            .map(function (data) { return data.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    CountersServiceProvider.prototype.getContraCounter = function (heroiId) {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_0__global__["a" /* GlobalVariable */].BASE_API + "counters/" + heroiId + "/contracounters")
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_5__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].throw(error); })
            .map(function (data) { return data.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    CountersServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Http */]])
    ], CountersServiceProvider);
    return CountersServiceProvider;
}());

//# sourceMappingURL=counters-service.js.map

/***/ }),

/***/ 379:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DicasServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__global__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__models_CustomError__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/*import { CustomError } from './../../models/CustomError';

  Generated class for the DicasServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var DicasServiceProvider = /** @class */ (function () {
    function DicasServiceProvider(http) {
        this.http = http;
        console.log('Hello DicasServiceProvider Provider');
    }
    DicasServiceProvider.prototype.getDicas = function () {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_4__global__["a" /* GlobalVariable */].BASE_API + "Dicas")
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_5__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error); })
            .map(function (data) { return data.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    DicasServiceProvider.prototype.getDica = function (id) {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_4__global__["a" /* GlobalVariable */].BASE_API + "Dicas/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_5__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return response.json().data; })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    DicasServiceProvider.prototype.addDica = function (Dica) {
        var rel = JSON.stringify(Dica, [
            'descricao',
            'heroiId',
            'usuarioId',
        ]);
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        return this.http.post(__WEBPACK_IMPORTED_MODULE_4__global__["a" /* GlobalVariable */].BASE_API + "Dicas", rel, { headers: header })
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_5__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) {
            return response.json();
        })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    DicasServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], DicasServiceProvider);
    return DicasServiceProvider;
}());

//# sourceMappingURL=dicas-service.js.map

/***/ }),

/***/ 380:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BuildFavoritaServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__models_CustomError__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__sessao_sessao__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__global__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_BuildFavorita__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/*
  Generated class for the BuildFavoritaServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var BuildFavoritaServiceProvider = /** @class */ (function () {
    function BuildFavoritaServiceProvider(http, sessao) {
        this.http = http;
        this.sessao = sessao;
        console.log('Hello BuildFavoritaServiceProvider Provider');
    }
    BuildFavoritaServiceProvider.prototype.getBuildFavoritas = function () {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__global__["a" /* GlobalVariable */].BASE_API + "BuildFavoritas")
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_0__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return _this.sessao.tratarRequisicao(response); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    BuildFavoritaServiceProvider.prototype.getBuildFavorita = function (id) {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__global__["a" /* GlobalVariable */].BASE_API + "BuildFavoritas/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_0__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return _this.sessao.tratarRequisicao(response); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    BuildFavoritaServiceProvider.prototype.addBuildFavorita = function (buildFavorita) {
        var _this = this;
        var usu = buildFavorita.toJson();
        var header = new __WEBPACK_IMPORTED_MODULE_6__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__global__["a" /* GlobalVariable */].BASE_API + "BuildFavoritas", usu, { headers: header })
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_0__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return new __WEBPACK_IMPORTED_MODULE_4__models_BuildFavorita__["a" /* BuildFavorita */](_this.sessao.tratarRequisicao(response)); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    BuildFavoritaServiceProvider.prototype.deleteBuildFavorita = function (id) {
        var _this = this;
        return this.http.delete(__WEBPACK_IMPORTED_MODULE_2__global__["a" /* GlobalVariable */].BASE_API + "BuildFavoritas/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_0__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return _this.sessao.tratarRequisicao(response); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    BuildFavoritaServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_5__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_1__sessao_sessao__["a" /* SessaoProvider */]])
    ], BuildFavoritaServiceProvider);
    return BuildFavoritaServiceProvider;
}());

//# sourceMappingURL=build-favorita-service.js.map

/***/ }),

/***/ 381:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BuildFavorita; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_sessao_sessao__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_app_module__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__BaseModel__ = __webpack_require__(36);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();



var BuildFavorita = /** @class */ (function (_super) {
    __extends(BuildFavorita, _super);
    function BuildFavorita(init) {
        var _this = _super.call(this, __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].injector.get(__WEBPACK_IMPORTED_MODULE_0__providers_sessao_sessao__["a" /* SessaoProvider */])) || this;
        Object.assign(_this, init);
        return _this;
    }
    BuildFavorita.prototype.getToJsonProperties = function () {
        return [
            'buildId',
            'usuarioId'
        ];
    };
    return BuildFavorita;
}(__WEBPACK_IMPORTED_MODULE_2__BaseModel__["a" /* BaseModel */]));

//# sourceMappingURL=BuildFavorita.js.map

/***/ }),

/***/ 382:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AmizadeServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__models_CustomError__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__sessao_sessao__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__global__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__usuario_service_usuario_service__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/*
  Generated class for the AmizadeServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var AmizadeServiceProvider = /** @class */ (function () {
    function AmizadeServiceProvider(http, sessao, events, apiUsuario) {
        var _this = this;
        this.http = http;
        this.sessao = sessao;
        this.events = events;
        this.apiUsuario = apiUsuario;
        console.log('Hello LeagueoflegendsServiceProvider Provider');
        __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000, 10000)
            .subscribe(function () { return _this.events.publish("usuarios:online", _this.getAmizadesOnline(_this.apiUsuario.getUsuarioLogado().usuarioId)); });
    }
    AmizadeServiceProvider.prototype.getAmizades = function () {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_6__global__["a" /* GlobalVariable */].BASE_API + "Amizades")
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_0__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return _this.sessao.tratarRequisicao(response); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    AmizadeServiceProvider.prototype.getAmizadesUsuarios = function (id) {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_6__global__["a" /* GlobalVariable */].BASE_API + "Amizades/GetAmizadesUsuarios/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_0__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return _this.sessao.tratarRequisicao(response); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    AmizadeServiceProvider.prototype.getAmizadesOnline = function (id) {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_6__global__["a" /* GlobalVariable */].BASE_API + "Amizades/GetAmigosOnline/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_0__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return _this.sessao.tratarRequisicao(response); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    AmizadeServiceProvider.prototype.addAmizade = function (lol) {
        var _this = this;
        var lolJson = lol.toJson();
        console.log(lolJson);
        var header = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        return this.http.post(__WEBPACK_IMPORTED_MODULE_6__global__["a" /* GlobalVariable */].BASE_API + "Amizades/AdicionarAmizade", lolJson, { headers: header })
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_0__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return _this.sessao.tratarRequisicao(response); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    AmizadeServiceProvider.prototype.deleteAmizade = function (id) {
        var _this = this;
        return this.http.delete(__WEBPACK_IMPORTED_MODULE_6__global__["a" /* GlobalVariable */].BASE_API + "Amizades/DeleteAmizades/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_0__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return _this.sessao.tratarRequisicao(response); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    AmizadeServiceProvider.prototype.deleteTodasAmizades = function (id) {
        var _this = this;
        return this.http.delete(__WEBPACK_IMPORTED_MODULE_6__global__["a" /* GlobalVariable */].BASE_API + "Amizades/DeleteTodasAmizades/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_0__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return _this.sessao.tratarRequisicao(response); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    AmizadeServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_5__sessao_sessao__["a" /* SessaoProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */],
            __WEBPACK_IMPORTED_MODULE_7__usuario_service_usuario_service__["a" /* UsuarioServiceProvider */]])
    ], AmizadeServiceProvider);
    return AmizadeServiceProvider;
}());

//# sourceMappingURL=amizade-service.js.map

/***/ }),

/***/ 383:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TimeServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__global__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(165);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__sessao_sessao__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__models_CustomError__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/*
  Generated class for the TimeServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var TimeServiceProvider = /** @class */ (function () {
    function TimeServiceProvider(http, sessao) {
        this.http = http;
        this.sessao = sessao;
        console.log('Hello TimeServiceProvider Provider');
    }
    TimeServiceProvider.prototype.getTimeUsuario = function (id) {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_1__global__["a" /* GlobalVariable */].BASE_API + "Times/GetTimeUsuario/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_5__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_0_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_0_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return _this.sessao.tratarRequisicao(response); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_0_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    TimeServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_4__sessao_sessao__["a" /* SessaoProvider */]])
    ], TimeServiceProvider);
    return TimeServiceProvider;
}());

//# sourceMappingURL=time-service.js.map

/***/ }),

/***/ 384:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeagueoflegendsRegioesServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__global__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__models_CustomError__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/*
  Generated class for the LeagueOfLegendsRegioesServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var LeagueoflegendsRegioesServiceProvider = /** @class */ (function () {
    function LeagueoflegendsRegioesServiceProvider(http) {
        this.http = http;
        console.log('Hello LeagueOfLegends_RegiaoServiceProvider Provider');
    }
    LeagueoflegendsRegioesServiceProvider.prototype.getLeagueOfLegends_Regioes = function () {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_0__global__["a" /* GlobalVariable */].BASE_API + "LeagueOfLegends_Regioes")
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_5__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    LeagueoflegendsRegioesServiceProvider.prototype.getLeagueOfLegends_Regiao = function (id) {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_0__global__["a" /* GlobalVariable */].BASE_API + "LeagueOfLegends_Regioes/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_5__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    LeagueoflegendsRegioesServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]])
    ], LeagueoflegendsRegioesServiceProvider);
    return LeagueoflegendsRegioesServiceProvider;
}());

//# sourceMappingURL=leagueoflegends-regioes-service.js.map

/***/ }),

/***/ 385:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeiticoServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_radix_trie_js__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_radix_trie_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_radix_trie_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__global__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__models_CustomError__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/*
  Generated class for the FeiticoServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var FeiticoServiceProvider = /** @class */ (function () {
    function FeiticoServiceProvider(http) {
        this.http = http;
        console.log("Hello FeiticoServiceProvider Provider");
    }
    FeiticoServiceProvider.prototype.getFeiticos = function () {
        var _this = this;
        if (this.tree)
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].create(function (observer) {
                observer.next(Array.from(_this.tree.values()));
                observer.complete();
            });
        return this.http
            .get(__WEBPACK_IMPORTED_MODULE_5__global__["a" /* GlobalVariable */].BASE_API + "Feiticos")
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_6__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error); })
            .map(function (data) {
            var feiticos = data.json();
            _this.tree = new __WEBPACK_IMPORTED_MODULE_4_radix_trie_js__();
            // Adiciona os elementos na arvore
            feiticos.forEach(function (a) {
                if (!a.nome) {
                    console.log(a);
                }
                else {
                    _this.tree.add(a.nome, a);
                }
            });
            return feiticos;
        })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error || "Server error"); });
    };
    FeiticoServiceProvider.prototype.getFeitico = function (id) {
        return this.http
            .get(__WEBPACK_IMPORTED_MODULE_5__global__["a" /* GlobalVariable */].BASE_API + "Feiticos/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_6__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error); })
            .map(function (response) { return response.json(); })
            .catch(function (error) {
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || "Server error");
        });
    };
    /**
     * @param  {string} query
     * @returns Observable
     */
    FeiticoServiceProvider.prototype.getSearch = function (query) {
        var _this = this;
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].create(function (observer) {
            observer.next(query.length > 0
                ? Array.from(_this.tree.fuzzyGet(query)).map(function (a) { return a[1]; })
                : Array.from(_this.tree.values()));
            observer.complete();
        });
    };
    FeiticoServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], FeiticoServiceProvider);
    return FeiticoServiceProvider;
}());

//# sourceMappingURL=feitico-service.js.map

/***/ }),

/***/ 386:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RunaServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_radix_trie_js__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_radix_trie_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_radix_trie_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__global__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__models_CustomError__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/*
  Generated class for the RunaServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var RunaServiceProvider = /** @class */ (function () {
    function RunaServiceProvider(http) {
        this.http = http;
        console.log("Hello RunaServiceProvider Provider");
    }
    RunaServiceProvider.prototype.getRunas = function () {
        var _this = this;
        if (this.tree)
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].create(function (observer) {
                observer.next(Array.from(_this.tree.values()));
                observer.complete();
            });
        return this.http
            .get(__WEBPACK_IMPORTED_MODULE_5__global__["a" /* GlobalVariable */].BASE_API + "Runas")
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_6__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error); })
            .map(function (data) {
            var runas = data.json();
            _this.tree = new __WEBPACK_IMPORTED_MODULE_4_radix_trie_js__();
            // Adiciona os elementos na arvore
            runas.forEach(function (a) {
                if (!a.nome) {
                    console.log(a);
                }
                else {
                    _this.tree.add(a.nome, a);
                }
            });
            return runas;
        })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error || "Server error"); });
    };
    RunaServiceProvider.prototype.getRuna = function (id) {
        return this.http
            .get(__WEBPACK_IMPORTED_MODULE_5__global__["a" /* GlobalVariable */].BASE_API + "Runas/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_6__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error); })
            .map(function (response) { return response.json(); })
            .catch(function (error) {
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json().error || "Server error");
        });
    };
    /**
     * @param  {string} query
     * @returns Observable
     */
    RunaServiceProvider.prototype.getSearch = function (query) {
        var _this = this;
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].create(function (observer) {
            observer.next(query.length > 0
                ? Array.from(_this.tree.fuzzyGet(query)).map(function (a) { return a[1]; })
                : Array.from(_this.tree.values()));
            observer.complete();
        });
    };
    RunaServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], RunaServiceProvider);
    return RunaServiceProvider;
}());

//# sourceMappingURL=runa-service.js.map

/***/ }),

/***/ 387:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(31);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsuarioServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular_util_events__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__leagueoflegends_service_leagueoflegends_service__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(318);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_catch__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__global__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__models_Usuario__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__sessao_sessao__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__models_CustomError__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};












/*
  Generated class for the UsuarioServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var UsuarioServiceProvider = /** @class */ (function () {
    function UsuarioServiceProvider(http, storage, apiLol, apiSessao, events) {
        var _this = this;
        this.http = http;
        this.storage = storage;
        this.apiLol = apiLol;
        this.apiSessao = apiSessao;
        this.events = events;
        this.headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        console.log('Hello UsuarioServiceProvider Provider');
        events.subscribe('sessao:refazer', function () {
            _this.login(_this.usuarioLogado.email, _this.usuarioLogado.senhaHash).subscribe(function (resp) {
                if (resp) {
                }
                else {
                    _this.logout();
                }
            }, function (err) { _this.logout(); }, function () { });
        });
        events.subscribe('sessao:deslogar', function () { return _this.logout(); });
    }
    UsuarioServiceProvider.prototype.getContadorSuperLikes = function () {
        return __awaiter(this, void 0, void 0, function () {
            var sl, novocontador;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.storage.get('superLikes')];
                    case 1:
                        sl = _a.sent();
                        if (!sl || sl == null || (new Date().getDay() - sl.data.getDay()) > 1) {
                            novocontador = { contador: 0, data: new Date };
                            this.storage.set('superLikes', novocontador);
                            return [2 /*return*/, novocontador];
                        }
                        return [2 /*return*/, sl];
                }
            });
        });
    };
    UsuarioServiceProvider.prototype.incrementaContadorSuperLikes = function () {
        return __awaiter(this, void 0, void 0, function () {
            var contador, novocontador;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getContadorSuperLikes()];
                    case 1:
                        contador = _a.sent();
                        novocontador = { contador: contador.contador + 1, data: new Date };
                        this.storage.set('superLikes', novocontador);
                        return [2 /*return*/];
                }
            });
        });
    };
    UsuarioServiceProvider.prototype.loginAutomatico = function () {
        var _this = this;
        // Or to get a key/value pair
        return this.storage.get('usuarioLogado').then(function (val) { return __awaiter(_this, void 0, void 0, function () {
            var usu;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!val) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.login(val.email, val.senhaHash).toPromise()];
                    case 1:
                        usu = _a.sent();
                        console.log("autenticado:", usu);
                        return [2 /*return*/, true];
                    case 2:
                        console.log('Nenhum usuario logado: ', val);
                        return [2 /*return*/, false];
                }
            });
        }); })
            .catch(function (err) {
            console.log("Erro ao logar" + err);
            return false;
        });
    };
    UsuarioServiceProvider.prototype.login = function (usuario, senha) {
        var _this = this;
        this.storage.remove('usuarioLogado');
        //TODO REMOVER EM Produção
        if (usuario == 'teste' && senha == 'teste') {
            return new __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"](function (observer) {
                var usuario = new __WEBPACK_IMPORTED_MODULE_9__models_Usuario__["a" /* Usuario */]({
                    usuarioId: 27,
                    email: 'viniciuslln@gmail.com',
                    tipoAtivacaoId: 2,
                    senhaHash: 'teste',
                    status: 'OK'
                });
                _this.usuarioLogado = usuario;
                _this.storage.set('usuarioLogado', usuario);
                observer.next(usuario);
            });
        }
        else
            return this.http.post(__WEBPACK_IMPORTED_MODULE_8__global__["a" /* GlobalVariable */].BASE_API + "usuarios/login", {
                "email": usuario,
                "senhahash": senha
            })
                .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_11__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
                .map(function (response) {
                var usuario = response.json();
                if (usuario && usuario.status == 'OK') {
                    _this.usuarioLogado = usuario;
                    _this.storage.set('usuarioLogado', usuario);
                    _this.apiSessao.atualizarChave(usuario.chaveSessao);
                }
                return usuario;
            })
                .catch(function (error) {
                _this.storage.remove('usuarioLogado');
                return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error');
            });
    };
    UsuarioServiceProvider.prototype.logout = function () {
        var _this = this;
        return new Promise(function () {
            _this.storage.remove('superLikes');
            _this.storage.remove('usuarioLogado');
            return true;
        });
    };
    UsuarioServiceProvider.prototype.getUsuario = function (id) {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_8__global__["a" /* GlobalVariable */].BASE_API + "usuarios/" + id)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_11__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) {
            return new __WEBPACK_IMPORTED_MODULE_9__models_Usuario__["a" /* Usuario */](_this.apiSessao.tratarRequisicao(response));
        })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    UsuarioServiceProvider.prototype.getUsuarioPorNome = function (nome) {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_8__global__["a" /* GlobalVariable */].BASE_API + "usuarios/GetUsuarioPorNome/" + nome)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_11__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) {
            return (_this.apiSessao.tratarRequisicao(response));
        })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    UsuarioServiceProvider.prototype.getUsuarioPorEmail = function (email) {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_8__global__["a" /* GlobalVariable */].BASE_API + "usuarios/GetUsuarioPorEmail/" + email)
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_11__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) {
            return (_this.apiSessao.tratarRequisicao(response));
        })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    UsuarioServiceProvider.prototype.getUsuarios = function () {
        var _this = this;
        return this.http.get(__WEBPACK_IMPORTED_MODULE_8__global__["a" /* GlobalVariable */].BASE_API + "usuarios")
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_11__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) {
            return _this.apiSessao.tratarRequisicao(response);
        })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error.json().error || 'Server error'); });
    };
    UsuarioServiceProvider.prototype.addUsuario = function (usuario) {
        var _this = this;
        var usu = usuario.toJson();
        console.log(usu);
        var header = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        return this.http.post(__WEBPACK_IMPORTED_MODULE_8__global__["a" /* GlobalVariable */].BASE_API + "usuarios/Cadastrar", usu, { headers: header })
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_11__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) {
            return new __WEBPACK_IMPORTED_MODULE_9__models_Usuario__["a" /* Usuario */](_this.apiSessao.tratarRequisicao(response));
        })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || 'Server error'); });
    };
    UsuarioServiceProvider.prototype.putUsuario = function (usuario) {
        var _this = this;
        var usu = usuario.toJson();
        console.log(usu);
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.put(__WEBPACK_IMPORTED_MODULE_8__global__["a" /* GlobalVariable */].BASE_API + "Usuarios/Editar/" + usuario.usuarioId, usu, { headers: headers })
            .retryWhen(function (error) { return (error instanceof __WEBPACK_IMPORTED_MODULE_11__models_CustomError__["a" /* CustomError */] && error.codigo == 100) ? __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].timer(2000) : __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error); })
            .map(function (response) {
            return new __WEBPACK_IMPORTED_MODULE_9__models_Usuario__["a" /* Usuario */](_this.apiSessao.tratarRequisicao(response));
        })
            .catch(function (error) {
            return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].throw(error || 'Server error');
        });
    };
    UsuarioServiceProvider.prototype.getUsuarioLogado = function () {
        return this.usuarioLogado;
    };
    UsuarioServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1__leagueoflegends_service_leagueoflegends_service__["a" /* LeagueoflegendsServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_10__sessao_sessao__["a" /* SessaoProvider */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular_util_events__["a" /* Events */]])
    ], UsuarioServiceProvider);
    return UsuarioServiceProvider;
}());

//# sourceMappingURL=usuario-service.js.map

/***/ }),

/***/ 688:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DadosPartida; });
/* unused harmony export Jogador */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_sessao_sessao__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_app_module__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__BaseModel__ = __webpack_require__(36);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();



var DadosPartida = /** @class */ (function (_super) {
    __extends(DadosPartida, _super);
    function DadosPartida(init) {
        var _this = _super.call(this, __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].injector.get(__WEBPACK_IMPORTED_MODULE_0__providers_sessao_sessao__["a" /* SessaoProvider */])) || this;
        Object.assign(_this, init);
        return _this;
    }
    DadosPartida.prototype.getToJsonProperties = function () {
        throw new Error("Method not implemented.");
    };
    return DadosPartida;
}(__WEBPACK_IMPORTED_MODULE_2__BaseModel__["a" /* BaseModel */]));

var Jogador = /** @class */ (function (_super) {
    __extends(Jogador, _super);
    function Jogador(init) {
        var _this = _super.call(this, __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].injector.get(__WEBPACK_IMPORTED_MODULE_0__providers_sessao_sessao__["a" /* SessaoProvider */])) || this;
        Object.assign(_this, init);
        return _this;
    }
    Jogador.prototype.getToJsonProperties = function () {
        throw new Error("Method not implemented.");
    };
    return Jogador;
}(__WEBPACK_IMPORTED_MODULE_2__BaseModel__["a" /* BaseModel */]));

//# sourceMappingURL=DadosPartida.js.map

/***/ }),

/***/ 689:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DadosNaoEstaticosUsuario; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_sessao_sessao__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_app_module__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__BaseModel__ = __webpack_require__(36);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();



var DadosNaoEstaticosUsuario = /** @class */ (function (_super) {
    __extends(DadosNaoEstaticosUsuario, _super);
    function DadosNaoEstaticosUsuario(init) {
        var _this = _super.call(this, __WEBPACK_IMPORTED_MODULE_1__app_app_module__["a" /* AppModule */].injector.get(__WEBPACK_IMPORTED_MODULE_0__providers_sessao_sessao__["a" /* SessaoProvider */])) || this;
        Object.assign(_this, init);
        return _this;
    }
    DadosNaoEstaticosUsuario.prototype.getToJsonProperties = function () {
        return [
            'desempenhoRecente',
            'rankInterno',
            'rankPositions',
            'desempenhoClasse'
        ];
    };
    return DadosNaoEstaticosUsuario;
}(__WEBPACK_IMPORTED_MODULE_2__BaseModel__["a" /* BaseModel */]));

//# sourceMappingURL=DadosNaoEstaticosUsuario.js.map

/***/ }),

/***/ 693:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BuildFeitico; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__BaseModel__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_sessao_sessao__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(31);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();



var BuildFeitico = /** @class */ (function (_super) {
    __extends(BuildFeitico, _super);
    function BuildFeitico(init) {
        var _this = _super.call(this, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].injector.get(__WEBPACK_IMPORTED_MODULE_1__providers_sessao_sessao__["a" /* SessaoProvider */])) || this;
        Object.assign(_this, init);
        return _this;
    }
    BuildFeitico.prototype.getToJsonProperties = function () {
        return [
            'feiticoId',
            'buildId'
        ];
    };
    return BuildFeitico;
}(__WEBPACK_IMPORTED_MODULE_0__BaseModel__["a" /* BaseModel */]));

//# sourceMappingURL=BuildFeitico.js.map

/***/ }),

/***/ 717:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_usuario_service_usuario_service__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular_util_events__ = __webpack_require__(88);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, usuarioService, events) {
        var _this = this;
        this.usuarioService = usuarioService;
        this.events = events;
        usuarioService.loginAutomatico().then(function (resp) { return resp ? _this.rootPage = 'HomePage' : _this.rootPage = 'WelcomePage'; }
        //resp => resp ? this.rootPage = 'ChampionUserbuildsPage' : this.rootPage = 'ChampionUserbuildsPage'
        )
            .catch(function () { return _this.rootPage = 'WelcomePage'; }
        //() => this.rootPage = ChampionBuildPage
        );
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
        // Subscribe to received  new message events
        this.events.subscribe('sessao:deslogar', function (msg) {
            _this.rootPage = 'WelcomePage';
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/media/vinih/564617C4783ECA80/Arquivos/Projetos/FMP/Front/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n\n'/*ion-inline-end:"/media/vinih/564617C4783ECA80/Arquivos/Projetos/FMP/Front/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_4__providers_usuario_service_usuario_service__["a" /* UsuarioServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular_util_events__["a" /* Events */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 718:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AutosizeDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the AutosizeDirective directive.
 *
 * See https://angular.io/api/core/Directive for more info on Angular
 * Directives.
 */
var AutosizeDirective = /** @class */ (function () {
    function AutosizeDirective(element) {
        this.element = element;
    }
    AutosizeDirective.prototype.onInput = function (textArea) {
        this.adjust();
    };
    AutosizeDirective.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () { return _this.adjust(); }, 0);
    };
    AutosizeDirective.prototype.adjust = function () {
        var textArea = this.element.nativeElement.getElementsByTagName('textarea')[0];
        textArea.style.overflow = 'hidden';
        textArea.style.height = 'auto';
        textArea.style.height = textArea.scrollHeight + "px";
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('input', ['$event.target']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [HTMLTextAreaElement]),
        __metadata("design:returntype", void 0)
    ], AutosizeDirective.prototype, "onInput", null);
    AutosizeDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: 'ion-textarea[autosize]'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]])
    ], AutosizeDirective);
    return AutosizeDirective;
}());

//# sourceMappingURL=autosize.js.map

/***/ }),

/***/ 719:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LaneServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the LaneServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var LaneServiceProvider = /** @class */ (function () {
    function LaneServiceProvider(http) {
        this.http = http;
        console.log('Hello LaneServiceProvider Provider');
    }
    LaneServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], LaneServiceProvider);
    return LaneServiceProvider;
}());

//# sourceMappingURL=lane-service.js.map

/***/ }),

/***/ 720:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TipoRelacionamentoServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the TipoRelacionamentoServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var TipoRelacionamentoServiceProvider = /** @class */ (function () {
    function TipoRelacionamentoServiceProvider(http) {
        this.http = http;
        console.log('Hello TipoRelacionamentoServiceProvider Provider');
    }
    TipoRelacionamentoServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], TipoRelacionamentoServiceProvider);
    return TipoRelacionamentoServiceProvider;
}());

//# sourceMappingURL=tipo-relacionamento-service.js.map

/***/ }),

/***/ 721:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProjetosProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the ProjetosProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var ProjetosProvider = /** @class */ (function () {
    function ProjetosProvider(http) {
        this.http = http;
        console.log('Hello ProjetosProvider Provider');
    }
    ProjetosProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], ProjetosProvider);
    return ProjetosProvider;
}());

//# sourceMappingURL=projetos.js.map

/***/ })

},[387]);
//# sourceMappingURL=main.js.map