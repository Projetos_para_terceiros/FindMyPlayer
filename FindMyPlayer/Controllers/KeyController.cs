﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FindMyPlayer.Controllers.Utils;
using FindMyPlayer.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FindMyPlayer.Controllers {
    [Produces( "application/json" )]
    [Route( "api/Key" )]
    public class KeyController : Controller {

        [HttpGet( "{chave}" )]
        public IActionResult TrocarChave([FromRoute] string chave) {
            using(var _context = new EntidadesContexto()) {
                _context.DeveloperLolKeyu.First().Key = chave;
                _context.SaveChanges();
            }

            return Ok( new Error { Codigo = 200, Mensagem = "Reinicie a aplicacao para concluir"} );
        }
    }
}