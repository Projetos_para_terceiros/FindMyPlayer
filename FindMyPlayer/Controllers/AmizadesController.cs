﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FindMyPlayer.Models;
using FindMyPlayer.Controllers.Utils;

namespace FindMyPlayer.Controllers {
    [Produces( "application/json" )]
    [Route( "api/Amizades" )]
    public class AmizadesController : Controller {
        private readonly EntidadesContexto _context;

        public AmizadesController( ) {
            _context = new EntidadesContexto();
        }

        // GET: api/Amizades
        [HttpGet]
        public IEnumerable<Amizade> GetAmizade() {
            return _context.Amizade;
        }

        // GET: api/Amizades/5
        [HttpGet( "GetAmizadesUsuarios/{id}" )]
        [Route("GetAmizadesUsuarios/")]
        public IActionResult GetAmizadesUsuario([FromRoute] int id) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var amizade = _context.Amizade.Where( m => m.Usuario1Id == id ).ToList();

            if (amizade == null) {
                return NotFound();
            }

            return Ok( amizade );
        }

      

        // POST: api/Amizades
        [HttpPost]
        [Route("AdicionarAmizade")]
        public async Task<IActionResult> AdicionarAmizade([FromBody] Amizade amizade) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var exist = await _context.Amizade.SingleOrDefaultAsync( a => a.Usuario1Id == amizade.Usuario1Id && a.Usuario2Id == amizade.Usuario2Id );
            if(exist != null) {
                return BadRequest( new Error { Codigo = 400, Mensagem = "Essa amizade ja existe" } );
            }


            _context.Amizade.Add( amizade );
            await _context.SaveChangesAsync();

            return CreatedAtAction( "GetAmizade", new { id = amizade.AmizadeId }, amizade );
        }

        // DELETE: api/Amizades/5
        [HttpDelete( "DeleteAmizade/{id}&{id2}" )]
        [Route( "DeleteAmizade/" )]
        public async Task<IActionResult> DeleteAmizade([FromRoute] int id, [FromRoute] int id2) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var amizade = await _context.Amizade.SingleOrDefaultAsync( m => m.AmizadeId == id );
            if (amizade == null) {
                return NotFound();
            }

            _context.Amizade.Remove( amizade );
            await _context.SaveChangesAsync();

            return Ok( amizade );
        }

        [HttpDelete( "DeleteTodasAmizades/{id}" )]
        [Route("DeleteTodasAmizades/")]
        public async Task<IActionResult> DeleteTodasAmizade([FromRoute] int id) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var amizade = _context.Amizade.Where( m => m.Usuario1Id == id ).ToList();
            if (amizade == null) {
                return NotFound();
            }

            _context.Amizade.RemoveRange( amizade );
            await _context.SaveChangesAsync();

            return Ok( amizade );
        }

        [HttpGet("GetStatus/{id}")]
        [Route("GetStatus/")]
        public IActionResult GetStatus( int id) {
            var online = Caminhos.usuariosConectados.SingleOrDefault( m => Convert.ToInt32( m ) == id );
            
            if(online == null) {
                return Ok( new Error { Codigo = 200, Mensagem = "Usuario offline" } );
            } else {
                return Ok( new Error { Codigo = 200, Mensagem = "Usuario online" } );
            }
        }

        [HttpGet("GetAmigosOnline/{id}")]
        [Route( "GetAmigosOnline/" )]
        public async Task<IActionResult> GetAmigosOnline(int id) {

            var listaAmigos = await _context.Amizade.Where( a => a.Usuario1Id == id ).ToListAsync();
            var listaUsuarios = Caminhos.usuariosConectados;

            var resp = new List<int>();

            foreach(var user in listaAmigos) {
                if (listaUsuarios.SingleOrDefault( a => Convert.ToUInt32(a) == user.Usuario2Id )  != null) {
                    resp.Add( user.Usuario2Id );
                }
            }

            return Ok( resp );

        }

        private bool AmizadeExists(int id) {
            return _context.Amizade.Any( e => e.AmizadeId == id );
        }
    }
}