using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FindMyPlayer.Models;

namespace FindMyPlayer.Controllers {
    [Produces( "application/json" )]
    [Route( "api/TipoAtivacoes" )]
    public class TipoAtivacoesController : Controller {
        private readonly EntidadesContexto _context;

        public TipoAtivacoesController() {
            _context = new EntidadesContexto( );
        }

        // GET: api/TipoAtivacoes
        [HttpGet]
        public IEnumerable<TipoAtivacao> GetTiposAtivacao() {
            return _context.TiposAtivacao;
        }

        // GET: api/TipoAtivacoes/5
        [HttpGet( "{id}" )]
        public async Task<IActionResult> GetTipoAtivacao( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var tipoAtivacao = await _context.TiposAtivacao.SingleOrDefaultAsync( m => m.TipoAtivacaoId == id );

            if (tipoAtivacao == null) {
                return NotFound( );
            }

            return Ok( tipoAtivacao );
        }

        // PUT: api/TipoAtivacoes/5
        [HttpPut( "{id}" )]
        public async Task<IActionResult> PutTipoAtivacao( [FromRoute] int id, [FromBody] TipoAtivacao tipoAtivacao ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            if (id != tipoAtivacao.TipoAtivacaoId) {
                return BadRequest( );
            }

            _context.Entry( tipoAtivacao ).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync( );
            } catch (DbUpdateConcurrencyException) {
                if (!TipoAtivacaoExists( id )) {
                    return NotFound( );
                } else {
                    throw;
                }
            }

            return NoContent( );
        }

        // POST: api/TipoAtivacoes
        [HttpPost]
        public async Task<IActionResult> PostTipoAtivacao( [FromBody] TipoAtivacao tipoAtivacao ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            _context.TiposAtivacao.Add( tipoAtivacao );
            await _context.SaveChangesAsync( );

            return CreatedAtAction( "GetTipoAtivacao", new { id = tipoAtivacao.TipoAtivacaoId }, tipoAtivacao );
        }

        // DELETE: api/TipoAtivacoes/5
        [HttpDelete( "{id}" )]
        public async Task<IActionResult> DeleteTipoAtivacao( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var tipoAtivacao = await _context.TiposAtivacao.SingleOrDefaultAsync( m => m.TipoAtivacaoId == id );
            if (tipoAtivacao == null) {
                return NotFound( );
            }

            _context.TiposAtivacao.Remove( tipoAtivacao );
            await _context.SaveChangesAsync( );

            return Ok( tipoAtivacao );
        }

        private bool TipoAtivacaoExists( int id ) {
            return _context.TiposAtivacao.Any( e => e.TipoAtivacaoId == id );
        }
    }
}