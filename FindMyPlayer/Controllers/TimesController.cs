﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FindMyPlayer.Controllers.Utils;
using FindMyPlayer.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FindMyPlayer.Controllers {
    [Produces( "application/json" )]
    [Route( "api/Times" )]
    public class TimesController : Controller {

        EntidadesContexto _context;

        public TimesController () {
            _context = new EntidadesContexto();
        }

        [HttpGet("GetTimeUsuario/{id}")]
        [Route("GetTimeUsuario/")]
        public IActionResult GetTimeUsuario( int id) {
            var usuariosConectados = Caminhos.usuariosConectados;
            var resp = new List<Usuario>();

            var usuarios = _context.Usuarios.Where( a => usuariosConectados.Contains( a.UsuarioId + ""));
            var t = usuarios.Select( a => a.LeagueOfLegendsId ).ToList();
            var lol_users = _context.LeagueOfLegends.Where( a => t.Contains( a.LeagueOfLegendsId)).ToList();

            if (lol_users.Count() < 5) {
                return BadRequest( new Error { Codigo = 404, Mensagem = "Nao ha pessoas suficientes conectadas" } );
            }

            var usuarioRequisitado = usuarios.SingleOrDefault( a => a.UsuarioId == id );
            var perfil = lol_users.SingleOrDefault( a => a.LeagueOfLegendsId == usuarioRequisitado.LeagueOfLegendsId );
            
            if(perfil.LanePrincipalId == 3) {
                resp.Add( _context.Usuarios.FirstOrDefault( m => m.LeagueOfLegendsId == lol_users.FirstOrDefault( a => a.LanePrincipalId == 1 ).LeagueOfLegendsId));
                resp.Add( _context.Usuarios.FirstOrDefault( m => m.LeagueOfLegendsId == lol_users.FirstOrDefault( a => a.LanePrincipalId == 2 ).LeagueOfLegendsId ) );
                resp.Add( _context.Usuarios.FirstOrDefault( m => m.LeagueOfLegendsId == lol_users.FirstOrDefault( a => a.LanePrincipalId == 3 ).LeagueOfLegendsId ) );
                resp.Add( _context.Usuarios.FirstOrDefault( m => m.LeagueOfLegendsId == lol_users.FirstOrDefault( a => a.LanePrincipalId == 4 ).LeagueOfLegendsId ) );
            } else if (perfil.LanePrincipalId == 1){
                resp.Add( _context.Usuarios.FirstOrDefault( m => m.LeagueOfLegendsId == lol_users.FirstOrDefault( a => a.LanePrincipalId == 2 ).LeagueOfLegendsId ) );
                var bot = lol_users.FirstOrDefault( a => a.LanePrincipalId == 3 );
                resp.Add( _context.Usuarios.FirstOrDefault( m => m.LeagueOfLegendsId ==  bot.LeagueOfLegendsId ) );
                lol_users.Remove( bot );
                resp.Add( _context.Usuarios.FirstOrDefault( m => m.LeagueOfLegendsId == lol_users.FirstOrDefault( a => a.LanePrincipalId == 3 ).LeagueOfLegendsId ) );
                resp.Add( _context.Usuarios.FirstOrDefault( m => m.LeagueOfLegendsId == lol_users.FirstOrDefault( a => a.LanePrincipalId == 4 ).LeagueOfLegendsId ) );
            } else if (perfil.LanePrincipalId == 2) {
                resp.Add( _context.Usuarios.FirstOrDefault( m => m.LeagueOfLegendsId == lol_users.FirstOrDefault( a => a.LanePrincipalId == 1 ).LeagueOfLegendsId ) );
                var bot = lol_users.FirstOrDefault( a => a.LanePrincipalId == 3 );
                resp.Add( _context.Usuarios.FirstOrDefault( m => m.LeagueOfLegendsId == bot.LeagueOfLegendsId ) );
                lol_users.Remove( bot );
                resp.Add( _context.Usuarios.FirstOrDefault( m => m.LeagueOfLegendsId == lol_users.FirstOrDefault( a => a.LanePrincipalId == 3 ).LeagueOfLegendsId ) );
                resp.Add( _context.Usuarios.FirstOrDefault( m => m.LeagueOfLegendsId == lol_users.FirstOrDefault( a => a.LanePrincipalId == 4 ).LeagueOfLegendsId ) );
            } else if (perfil.LanePrincipalId == 4) {
                resp.Add( _context.Usuarios.FirstOrDefault( m => m.LeagueOfLegendsId == lol_users.FirstOrDefault( a => a.LanePrincipalId == 1 ).LeagueOfLegendsId ) );
                resp.Add( _context.Usuarios.FirstOrDefault( m => m.LeagueOfLegendsId == lol_users.FirstOrDefault( a => a.LanePrincipalId == 2 ).LeagueOfLegendsId ) );
                var bot = lol_users.FirstOrDefault( a => a.LanePrincipalId == 3 );
                resp.Add( _context.Usuarios.FirstOrDefault( m => m.LeagueOfLegendsId == bot.LeagueOfLegendsId ) );
                lol_users.Remove( bot );
                resp.Add( _context.Usuarios.FirstOrDefault( m => m.LeagueOfLegendsId == lol_users.FirstOrDefault( a => a.LanePrincipalId == 3 ).LeagueOfLegendsId ) );
            }

            return Ok( resp );


        }
    }
}