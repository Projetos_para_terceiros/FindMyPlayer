﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FindMyPlayer.Models;

namespace FindMyPlayer.Controllers {
    [Produces( "application/json" )]
    [Route( "api/BuildFeiticos" )]
    public class BuildFeiticosController : Controller {
        private readonly EntidadesContexto _context;

        public BuildFeiticosController() {
            _context = new EntidadesContexto( );
        }

        // GET: api/BuildFeiticos
        [HttpGet]
        public IEnumerable<BuildFeitico> GetBuildFeiticos() {
            return _context.BuildFeiticos;
        }

        // GET: api/BuildFeiticos/5
        [HttpGet( "{id}" )]
        public async Task<IActionResult> GetBuildFeitico([FromRoute] int id) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var buildFeitico = await _context.BuildFeiticos.SingleOrDefaultAsync( m => m.BuildFeiticoId == id );

            if (buildFeitico == null) {
                return NotFound();
            }

            return Ok( buildFeitico );
        }

        // PUT: api/BuildFeiticos/5
        [HttpPut( "{id}" )]
        public async Task<IActionResult> PutBuildFeitico([FromRoute] int id, [FromBody] BuildFeitico buildFeitico) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            if (id != buildFeitico.BuildFeiticoId) {
                return BadRequest();
            }

            _context.Entry( buildFeitico ).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException) {
                if (!BuildFeiticoExists( id )) {
                    return NotFound();
                } else {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/BuildFeiticos
        [HttpPost]
        public async Task<IActionResult> PostBuildFeitico([FromBody] BuildFeitico buildFeitico) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            _context.BuildFeiticos.Add( buildFeitico );
            await _context.SaveChangesAsync();

            return CreatedAtAction( "GetBuildFeitico", new { id = buildFeitico.BuildFeiticoId }, buildFeitico );
        }

        // DELETE: api/BuildFeiticos/5
        [HttpDelete( "{id}" )]
        public async Task<IActionResult> DeleteBuildFeitico([FromRoute] int id) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var buildFeitico = await _context.BuildFeiticos.SingleOrDefaultAsync( m => m.BuildFeiticoId == id );
            if (buildFeitico == null) {
                return NotFound();
            }

            _context.BuildFeiticos.Remove( buildFeitico );
            await _context.SaveChangesAsync();

            return Ok( buildFeitico );
        }

        private bool BuildFeiticoExists(int id) {
            return _context.BuildFeiticos.Any( e => e.BuildFeiticoId == id );
        }
    }
}