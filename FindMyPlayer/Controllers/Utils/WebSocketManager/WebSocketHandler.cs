using FindMyPlayer.Controllers.Utils;
using FindMyPlayer.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.IO;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WebSocketManager {
    public abstract class WebSocketHandler {
        protected WebSocketConnectionManager WebSocketConnectionManager { get; set; }

        public WebSocketHandler(WebSocketConnectionManager webSocketConnectionManager) {
            WebSocketConnectionManager = webSocketConnectionManager;
        }

        public virtual async Task OnConnected(WebSocket socket) {
            var usuarioId =  await ReceiveStringAsync( socket ) ;
            Usuario exist = null;
            SessaoUsuario chaveOk = null;

            var jaOnline = Caminhos.usuariosConectados.SingleOrDefault( a => a == usuarioId );

            if (jaOnline != null) {
                await socket.CloseAsync( closeStatus: WebSocketCloseStatus.NormalClosure,
                                    statusDescription: "User already online.",
                                    cancellationToken: CancellationToken.None );
            } else {
                using (var context = new EntidadesContexto()) {
                    exist = await context.Usuarios.SingleOrDefaultAsync( a => a.UsuarioId == Convert.ToInt32( usuarioId ) );
                    chaveOk = await context.SessasoUsuario.SingleOrDefaultAsync( a => a.UsuarioId == exist.UsuarioId );
                }
                if (exist != null && chaveOk != null) {
                    WebSocketConnectionManager.AddSocket( socket, usuarioId );
                } else {
                    await socket.CloseAsync( closeStatus: WebSocketCloseStatus.NormalClosure,
                                        statusDescription: "User is not valid.",
                                        cancellationToken: CancellationToken.None );
                }
            }
        }

        public virtual async Task OnDisconnected(WebSocket socket) {
            await WebSocketConnectionManager.RemoveSocket( WebSocketConnectionManager.GetId( socket ) );
        }

        public async Task SendMessageAsync(WebSocket socket, string message) {
            if (socket.State != WebSocketState.Open)
                return;

            await socket.SendAsync( buffer: new ArraySegment<byte>( array: Encoding.ASCII.GetBytes( message ),
                                                                  offset: 0,
                                                                  count: message.Length ),
                                   messageType: WebSocketMessageType.Text,
                                   endOfMessage: true,
                                   cancellationToken: CancellationToken.None );
        }

        public async Task SendMessageAsync(string socketId, string message) {
            await SendMessageAsync( WebSocketConnectionManager.GetSocketById( socketId ), message );
        }

        public async Task SendMessageToAllAsync(string message) {
            foreach (var pair in WebSocketConnectionManager.GetAll()) {
                if (pair.Value.State == WebSocketState.Open)
                    await SendMessageAsync( pair.Value, message );
            }
        }

        //TODO - decide if exposing the message string is better than exposing the result and buffer
        public abstract Task ReceiveAsync(WebSocket socket, WebSocketReceiveResult result, byte[] buffer);

        private static async Task<string> ReceiveStringAsync(WebSocket socket, CancellationToken ct = default( CancellationToken )) {
            var buffer = new ArraySegment<byte>( new byte[ 8192 ] );
            using (var ms = new MemoryStream()) {
                WebSocketReceiveResult result;
                do {
                    ct.ThrowIfCancellationRequested();

                    result = await socket.ReceiveAsync( buffer, ct );
                    ms.Write( buffer.Array, buffer.Offset, result.Count );
                }
                while (!result.EndOfMessage);

                ms.Seek( 0, SeekOrigin.Begin );
                if (result.MessageType != WebSocketMessageType.Text) {
                    return null;
                }

                // Encoding UTF8: https://tools.ietf.org/html/rfc6455#section-5.6
                using (var reader = new StreamReader( ms, Encoding.UTF8 )) {
                    return await reader.ReadToEndAsync();
                }
            }
        }
    }
}