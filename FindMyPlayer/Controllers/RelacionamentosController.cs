using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FindMyPlayer.Models;
using Microsoft.AspNetCore.Cors;

namespace FindMyPlayer.Controllers {
    [Produces( "application/json" )]
    [Route( "api/Relacionamentos" )]
    public class RelacionamentosController : Controller {
        private readonly EntidadesContexto _context;

        public RelacionamentosController() {
            _context = new EntidadesContexto( );
        }

        // GET: api/Relacionamentos
        [HttpGet]
        [DisableCors]
        public IEnumerable<Relacionamento> GetRelacionamentos() {
            return _context.Relacionamentos;
        }

        // GET: api/Relacionamentos/5
        [HttpGet( "{id}" )]
        [DisableCors]
        public async Task<IActionResult> GetRelacionamento( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var relacionamento = await _context.Relacionamentos.SingleOrDefaultAsync( m => m.RelacionamentoId == id );

            if (relacionamento == null) {
                return NotFound( );
            }

            return Ok( relacionamento );
        }

        // PUT: api/Relacionamentos/5
        [HttpPut( "{id}" )]
        [DisableCors]
        public async Task<IActionResult> PutRelacionamento( [FromRoute] int id, [FromBody] Relacionamento relacionamento ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            if (id != relacionamento.RelacionamentoId) {
                return BadRequest( );
            }

            _context.Entry( relacionamento ).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync( );
            } catch (DbUpdateConcurrencyException) {
                if (!RelacionamentoExists( id )) {
                    return NotFound( );
                } else {
                    throw;
                }
            }

            return NoContent( );
        }

        // POST: api/Relacionamentos
        [HttpPost]
        [DisableCors]
        public async Task<IActionResult> PostRelacionamento( [FromBody] Relacionamento relacionamento ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            _context.Relacionamentos.Add( relacionamento );
            await _context.SaveChangesAsync( );

            return CreatedAtAction( "GetRelacionamento", new { id = relacionamento.RelacionamentoId }, relacionamento );
        }

        // DELETE: api/Relacionamentos/5
        [HttpDelete( "{id}" )]
        [DisableCors]
        public async Task<IActionResult> DeleteRelacionamento( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var relacionamento = await _context.Relacionamentos.SingleOrDefaultAsync( m => m.RelacionamentoId == id );
            if (relacionamento == null) {
                return NotFound( );
            }

            _context.Relacionamentos.Remove( relacionamento );
            await _context.SaveChangesAsync( );

            return Ok( relacionamento );
        }

        [HttpGet( "GetNaoRelacionados/{id}" )]
        [Route( "GetNaoRelacionados/" )]
        [DisableCors]
        public async Task<IActionResult> GetNaoRelacionados( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }
            var usuario = _context.Usuarios.Where( a => a.LeagueOfLegendsId != null ).ToList();
            var relacionamento = await _context.Relacionamentos.Where( m => m.Usuario1Id == id ).Select( a => a.Usuario2Id ).ToListAsync();
            var resp = usuario.Where( a => !relacionamento.Contains( a.UsuarioId ) && a.UsuarioId != id ).ToList();
            return Ok( resp );
        }

        [HttpGet( "GetFavoritadas/{id}" )]
        [Route( "GetFavoritadas/" )]
        [DisableCors]
        public async Task<IActionResult> GetFavoritadas( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }
            var usuario = _context.Usuarios.ToList( );
            var relacionamento = await _context.Relacionamentos.Where( m => m.Usuario1Id == id && m.TipoRelacionamentoId == 1).Select( a => a.Usuario2Id ).ToListAsync( );
            var resp = usuario.Where( a => relacionamento.Contains( a.UsuarioId ) ).ToList();
            return Ok( relacionamento );
        }

        [HttpGet( "GetNaoFavoritadas/{id}" )]
        [Route( "GetNaoFavoritadas/" )]
        [DisableCors]
        public async Task<IActionResult> GetNaoFavoritadas( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }
            var usuario = _context.Usuarios.ToList( );
            var relacionamento = await _context.Relacionamentos.Where( m => m.Usuario1Id == id && m.TipoRelacionamentoId == 2 ).Select( a => a.Usuario2Id ).ToListAsync( );
            var resp = usuario.Where( a => relacionamento.Contains( a.UsuarioId ) ).ToList( );
            return Ok( relacionamento );
        }

        private bool RelacionamentoExists( int id ) {
            return _context.Relacionamentos.Any( e => e.RelacionamentoId == id );
        }
    }
}