using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FindMyPlayer.Models;

namespace FindMyPlayer.Controllers {
    [Produces( "application/json" )]
    [Route( "api/TipoRelacionamentos" )]
    public class TipoRelacionamentosController : Controller {
        private readonly EntidadesContexto _context;

        public TipoRelacionamentosController() {
            _context = new EntidadesContexto( );
        }

        // GET: api/TipoRelacionamentos
        [HttpGet]
        public IEnumerable<TipoRelacionamento> GetTipoRelacionamentos() {
            return _context.TipoRelacionamentos;
        }

        // GET: api/TipoRelacionamentos/5
        [HttpGet( "{id}" )]
        public async Task<IActionResult> GetTipoRelacionamento( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var tipoRelacionamento = await _context.TipoRelacionamentos.SingleOrDefaultAsync( m => m.TipoRelacionamentoId == id );

            if (tipoRelacionamento == null) {
                return NotFound( );
            }

            return Ok( tipoRelacionamento );
        }

        // PUT: api/TipoRelacionamentos/5
        [HttpPut( "{id}" )]
        public async Task<IActionResult> PutTipoRelacionamento( [FromRoute] int id, [FromBody] TipoRelacionamento tipoRelacionamento ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            if (id != tipoRelacionamento.TipoRelacionamentoId) {
                return BadRequest( );
            }

            _context.Entry( tipoRelacionamento ).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync( );
            } catch (DbUpdateConcurrencyException) {
                if (!TipoRelacionamentoExists( id )) {
                    return NotFound( );
                } else {
                    throw;
                }
            }

            return NoContent( );
        }

        // POST: api/TipoRelacionamentos
        [HttpPost]
        public async Task<IActionResult> PostTipoRelacionamento( [FromBody] TipoRelacionamento tipoRelacionamento ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            _context.TipoRelacionamentos.Add( tipoRelacionamento );
            await _context.SaveChangesAsync( );

            return CreatedAtAction( "GetTipoRelacionamento", new { id = tipoRelacionamento.TipoRelacionamentoId }, tipoRelacionamento );
        }

        // DELETE: api/TipoRelacionamentos/5
        [HttpDelete( "{id}" )]
        public async Task<IActionResult> DeleteTipoRelacionamento( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var tipoRelacionamento = await _context.TipoRelacionamentos.SingleOrDefaultAsync( m => m.TipoRelacionamentoId == id );
            if (tipoRelacionamento == null) {
                return NotFound( );
            }

            _context.TipoRelacionamentos.Remove( tipoRelacionamento );
            await _context.SaveChangesAsync( );

            return Ok( tipoRelacionamento );
        }

        private bool TipoRelacionamentoExists( int id ) {
            return _context.TipoRelacionamentos.Any( e => e.TipoRelacionamentoId == id );
        }
    }
}