﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FindMyPlayer.Models;

namespace FindMyPlayer.Controllers {
    [Produces( "application/json" )]
    [Route( "api/Runas" )]
    public class RunasController : Controller {
        private readonly EntidadesContexto _context;

        public RunasController() {
            _context = new EntidadesContexto( );
        }

        // GET: api/Runas
        [HttpGet]
        public IEnumerable<Runa> GetRunas() {
            return _context.Runas;
        }

        // GET: api/Runas/5
        [HttpGet( "{id}" )]
        public async Task<IActionResult> GetRuna([FromRoute] int id) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var runa = await _context.Runas.SingleOrDefaultAsync( m => m.ItemId == id );

            if (runa == null) {
                return NotFound();
            }

            return Ok( runa );
        }

        // PUT: api/Runas/5
        [HttpPut( "{id}" )]
        public async Task<IActionResult> PutRuna([FromRoute] int id, [FromBody] Runa runa) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            if (id != runa.ItemId) {
                return BadRequest();
            }

            _context.Entry( runa ).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException) {
                if (!RunaExists( id )) {
                    return NotFound();
                } else {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Runas
        [HttpPost]
        public async Task<IActionResult> PostRuna([FromBody] Runa runa) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            _context.Runas.Add( runa );
            await _context.SaveChangesAsync();

            return CreatedAtAction( "GetRuna", new { id = runa.ItemId }, runa );
        }

        // DELETE: api/Runas/5
        [HttpDelete( "{id}" )]
        public async Task<IActionResult> DeleteRuna([FromRoute] int id) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var runa = await _context.Runas.SingleOrDefaultAsync( m => m.ItemId == id );
            if (runa == null) {
                return NotFound();
            }

            _context.Runas.Remove( runa );
            await _context.SaveChangesAsync();

            return Ok( runa );
        }

        private bool RunaExists(int id) {
            return _context.Runas.Any( e => e.ItemId == id );
        }
    }
}