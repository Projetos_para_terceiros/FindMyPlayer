﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {
    [Table("tb_avaliacao_usuario")]
    public class AvaliacaoUsuario {
        [Key, Column("avaliacao_usuario_id")]
        public int AvaliacaoUsuarioId { get; set; }

        [Column("estrelas")]
        public int Estrelas { get; set; }

        [Column("avaliacao")]
        public string Avaliacao { get; set; }

        [Column("usuario_avaliado_id")]
        public int UsuarioAvaliadoId { get; set; }
        public Usuario UsuarioAvaliado { get; set; }

        [Column( "usuario_avaliador_id" )]
        public int UsuarioAvaliadorId { get; set; }
        public Usuario UsuarioAvaliador { get; set; }

        [Column("data_hora")]
        public DateTime DataHora { get; set; }

    }
}
