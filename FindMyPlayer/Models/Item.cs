﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {
    [Table( "tb_item" )]
    public class Item : Base {
        [Key, Column( "item_id" )]
        public int ItemId { get; set; }
        [Column( "nome" )]
        public string Nome { get; set; }
        [Column( "descricao" )]
        public string Descricao { get; set; }
        [Column( "valor" )]
        public int Valor { get; set; }
        [Column( "imagem" )]
        public string Imagem { get; set; }

        [Column( "jogo_id" )]
        public int JogoId { get; set; }
        public Jogo Jogo { get; set; }
    }
}
