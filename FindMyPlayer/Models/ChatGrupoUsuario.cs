﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {
    [Table("tb_chat_grupo_usuario")]
    public class ChatGrupoUsuario {
        [Key, Column("chat_usuario_id")]
        public int ChatGrupoUsuarioId { get; set; }

        [Column( "usuario_id" )]
        public int UsuarioId { get; set; }
        public Usuario Usuario { get; set; }


        [Column("chat_grupo_id")]
        public int ChatGrupoId { get; set; }
        public ChatGrupo ChatGrupo { get; set; }
    }
}
