﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {
    [Table( "tb_league_of_legends_regiao" )]
    public class LeagueOfLegends_Regiao : Base {
        [Key, Column( "league_of_legends_id" )]
        public int LeagueOfLegends_RegiaoId { get; set; }
        [Column( "nome" )]
        public string Nome { get; set; }
    }
}
