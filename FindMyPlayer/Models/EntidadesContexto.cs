﻿using Microsoft.EntityFrameworkCore;
using FindMyPlayer.Models;

namespace FindMyPlayer.Models {
    public class EntidadesContexto : DbContext {
        public EntidadesContexto( DbContextOptions<EntidadesContexto> options ) : base( options ) { }

        public EntidadesContexto() { }

        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<LeagueOfLegends> LeagueOfLegends { get; set; }
        public DbSet<TipoAtivacao> TiposAtivacao { get; set; }

        public DbSet<Relacionamento> Relacionamentos { get; set; }
        public DbSet<TipoRelacionamento> TipoRelacionamentos { get; set; }

        public DbSet<Heroi> Herois { get; set; }

        public DbSet<Counter> Counters { get; set; }

        public DbSet<LeagueOfLegends_Regiao> LeagueOfLegends_Regiao { get; set; }

        public DbSet<Jogo> Jogos { get; set; }

        public DbSet<Build> Builds { get; set; }
        public DbSet<Item> Itens { get; set; }
        public DbSet<Runa> Runas { get; set; }
        public DbSet<Feitico> Feiticos { get; set; }

        public DbSet<BuildItem> BuildItens { get; set; }
        public DbSet<BuildRuna> BuildRunas { get; set; }
        public DbSet<BuildFeitico> BuildFeiticos { get; set; }

        public DbSet<BuildFavorita> BuildFavoritas { get; set; }

        public DbSet<Comentario> Comentarios { get; set; }

        public DbSet<Dica> Dicas { get; set; }

        public DbSet<Lane> Lanes { get; set; }

        public DbSet<ChatMessages> ChatMessages { get; set; }

        public DbSet<ChatStatus> ChatStatus { get; set; }

        public DbSet<SessaoUsuario> SessasoUsuario { get; set; }

        public DbSet<AvaliacaoUsuario> AvaliacaoUsuario { get; set; }

        public DbSet<Amizade> Amizade { get; set; }
        public DbSet<ChatGrupo> ChatGrupo { get; set; }
        public DbSet<ChatGrupoUsuario> ChatGrupoUsuario { get; set; }

        public DbSet<DeveloperLolKey> DeveloperLolKeyu { get; set; }

        protected override void OnModelCreating( ModelBuilder modelBuilder ) { }

        protected override void OnConfiguring( DbContextOptionsBuilder optionsBuilder ) {
            optionsBuilder.UseMySql(
                    //AZURE
                    //"Database=localdb;Data Source=127.0.0.1;Port=50252;User Id=azure;Password=6#vWHD_$",
                    //"Server=127.0.0.1;Port=50252;DataBase=localdb;Uid=azure;Pwd=6#vWHD_$",
                    //LOCAL
                    //"Server=localhost;DataBase=findmyplayer;Uid=root;Pwd=",
                    //HIROKU
                    "Database=s3npynhl8ymafywa;Data Source=thh2lzgakldp794r.cbetxkdyhwsb.us-east-1.rds.amazonaws.com;Port=3306;User Id=xxbtfoirmjb4u7zs;Password=jaqm35dwf0qblyj9",
                    b => b.MigrationsAssembly( "AspNetCoreMultipleProject" )
                 );
        }

    }
}
