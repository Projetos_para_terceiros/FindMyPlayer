﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {

    public enum TipoUsuario {
        FREE,
        PREMIUM
    }

    [Table( "tb_usuario" )]
    public class Usuario : Base {
        [Key, Column( "usuario_id" )]
        public int UsuarioId { get; set; }

        [Column("nome")]
        public string Nome { get; set; }

        [Column( "email" )]
        public string Email { get; set; }
        [Column( "senha_hash" )]
        public string SenhaHash { get; set; }

        [Column( "league_of_legends_id" )]
        public int? LeagueOfLegendsId { get; set; }
        public LeagueOfLegends LeagueOfLegends { get; set; }

        [Column( "tipo_ativacao_id" )]
        public int TipoAtivacaoId { get; set; }
        public TipoAtivacao TipoAtivacao { get; set; }

        [Column("tipo_usuario")]
        public TipoUsuario TipoUsuario { get; set; }

        [Column("data_compra")]
        public DateTime DataCompra { get; set; }

        [Column("data_cadastro")]
        public DateTime DataCadastro { get; set; }

        List<AvaliacaoUsuario> Avaliacoes { get; set; }
    }
}
