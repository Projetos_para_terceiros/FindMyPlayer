﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {
    [Table( "tb_build_feitico" )]
    public class BuildFeitico : Base{
        [Key, Column( "build_feitico_id" )]
        public int BuildFeiticoId { get; set; }

        [Column( "feitico_id" )]
        public int FeiticoId { get; set; }
        public Feitico Feitcio { get; set; }

        [Column( "build_id" )]
        public int BuildId { get; set; }
        public Build Build { get; set; }
    }
}
