﻿using FindMyPlayer.Controllers.Utils;
using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace FindMyPlayer.Models {
    public static class InicializaBD {
        public static async System.Threading.Tasks.Task InitializeAsync(  ) {
            using (var context = new EntidadesContexto()) {
                if (context.Database.EnsureCreated()) {

                    context.DeveloperLolKeyu.Add( new DeveloperLolKey { Key = "" } );

                    context.TiposAtivacao.Add( new TipoAtivacao() { Nome = "Ativado" } );
                    context.TiposAtivacao.Add( new TipoAtivacao() { Nome = "Pendente" } );

                    context.Lanes.Add( new Lane() { Nome = "Top" } );
                    context.Lanes.Add( new Lane() { Nome = "Middle" } );
                    context.Lanes.Add( new Lane() { Nome = "Botton" } );
                    context.Lanes.Add( new Lane() { Nome = "Jungle" } );

                    context.TipoRelacionamentos.Add( new TipoRelacionamento() { Nome = "Gostei" } );
                    context.TipoRelacionamentos.Add( new TipoRelacionamento() { Nome = "Super Like" } );
                    context.TipoRelacionamentos.Add( new TipoRelacionamento() { Nome = "Não Gostei" } );

                    context.ChatStatus.Add( new ChatStatus() { Nome = "Enviado" } );
                    context.ChatStatus.Add( new ChatStatus() { Nome = "Recebido" } );
                    context.ChatStatus.Add( new ChatStatus() { Nome = "Lido" } );

                    context.LeagueOfLegends_Regiao.Add( new LeagueOfLegends_Regiao() { Nome = "Brasil" } );

                    context.Jogos.Add( new Jogo() { Nome = "League of Legends" } );

                    context.SaveChanges();
                }
                if (context.Itens.Count() == 0) {
                    var itens = await LeagueOfLegendsAPI.RetornarItensAsync();
                    foreach (var x in itens.data) {
                        try {
                            context.Itens.Add( new Item() {
                                Nome = x.Value.name,
                                Descricao = x.Value.description,
                                Valor = x.Value.gold.Base,
                                Imagem = x.Value.image.full,
                                JogoId = 1
                            } );
                        } catch { }
                    }
                }
                if (context.Runas.Count() == 0) {
                    var runas = await LeagueOfLegendsAPI.RetornarRunasAsync();
                    foreach (var x in runas.data) {
                        try {
                            context.Runas.Add( new Runa() {
                                Nome = x.Value.name,
                                Descricao = x.Value.description,
                                Imagem = x.Value.image.full,
                                JogoId = 1
                            } );
                        } catch { }
                    }
                }
                if (context.Feiticos.Count() == 0) {
                    var feiticos = await LeagueOfLegendsAPI.RetornarFeiticosAsync();
                    foreach (var x in feiticos.data) {
                        try {
                            context.Feiticos.Add( new Feitico() {
                                Nome = x.Value.name,
                                Descricao = x.Value.description,
                                Imagem = x.Value.image.full,
                                JogoId = 1
                            } );
                        } catch { }
                    }
                }
                if (context.Herois.Count() == 0) {
                    var herois = LeagueOfLegendsAPI.RetornarHerois();
                    foreach (var x in herois.data) {
                        context.Herois.Add( new Heroi() {
                            Nome = x.Value.name,
                            Titulo = x.Value.title,
                            Historia = x.Value.lore,
                            ChampionId = x.Value.id,
                            Imagem = x.Value.image.full,
                            JogoId = 1
                        } );
                    }
                }
                    context.SaveChanges();
                }
            Caminhos.usuariosConectados = new System.Collections.Generic.List<string>();
            Procedimentos.AtualizarRankInterno();
            }
        }
    }

